# README #

This is an Eclipse Project for RMIT Programming Project 1 - Behaviour Simulator

* Quick summary
* Version 0.2

### How do I get set up? ###
* In Eclipse:
* "Import existing project into workspace"

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: s3392244@student.rmit.edu.au
* Other community or team contact: andy.song@rmit.edu.au