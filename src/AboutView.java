
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

/**
 * 
 * @author anthonyrawlins
 *
 */
public class AboutView extends JFrame {

   /**
    * 
    */
   private static final long serialVersionUID = 9190776061696757585L;

   /**
    * 
    */
   public AboutView() {
      getContentPane().setBackground(Color.BLACK);

      setUndecorated(true);
      setResizable(false);
      setSize(new Dimension(690, 360));
      setPreferredSize(new Dimension(690, 360));
      try {
         Point cp = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
         setLocation(new Point(cp.x - 345, cp.y - 180));
      } catch (HeadlessException e) {

      }

      getContentPane().setPreferredSize(new Dimension(690, 360));
      getContentPane().setBounds(new Rectangle(0, 0, 690, 0));
      getContentPane().setLayout(null);

      JTextPane txtpnTraitsVA = new JTextPane();
      txtpnTraitsVA.setForeground(Color.GRAY);
      txtpnTraitsVA.setDragEnabled(false);
      txtpnTraitsVA.setEditable(false);
      txtpnTraitsVA.setBackground(Color.BLACK);
      txtpnTraitsVA.setText(
            "A Group Behaviour Simulation Tool\n\nTechnical Lead, UI, & AI: Anthony Rawlins\nAdditional Programming & Testing: Ben Tierney\nProject Manager: Glenn Florence\nConsultant: Dr. Andy Song\n\nOpen Universities Australia\n\n©2015 RMIT University");

      txtpnTraitsVA.setBounds(352, 64, 297, 206);
      getContentPane().add(txtpnTraitsVA);

      JLabel label = new JLabel("");
      label.setIcon(new ImageIcon(AboutView.class.getResource("/traits-logo.png")));
      label.setBounds(16, 20, 283, 281);
      getContentPane().add(label);

      JButton button = new CloseControl();
      button.setIcon(new ImageIcon(AboutView.class.getResource("/gray/circle-x-2x.png")));
      button.setBounds(6, 6, 32, 32);
      getContentPane().add(button);

      JTextPane txtpnOpenIconicCc = new JTextPane();
      txtpnOpenIconicCc.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
      txtpnOpenIconicCc.setEditable(false);
      txtpnOpenIconicCc.setForeground(Color.GRAY);
      txtpnOpenIconicCc.setBackground(Color.BLACK);
      txtpnOpenIconicCc.setText("Open Iconic — www.useiconic.com/open\nProcessing  2.2.1 — processing.org");
      txtpnOpenIconicCc.setBounds(352, 282, 297, 49);
      getContentPane().add(txtpnOpenIconicCc);

      JLabel version = new JLabel("v0.3.312");
      version.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
      version.setForeground(Color.GRAY);
      version.setBackground(Color.BLACK);
      version.setBounds(91, 202, 61, 16);
      getContentPane().add(version);
   }

   private class CloseControl extends JButton {
      /**
       * 
       */
      private static final long serialVersionUID = -1381155464607511108L;

      public CloseControl() {
         setBorderPainted(false);
         setContentAreaFilled(false);
         setFocusPainted(false);
         setOpaque(false);
         addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               SwingUtilities.getWindowAncestor(CloseControl.this).dispose();
            }
         });
      }
   }
}
