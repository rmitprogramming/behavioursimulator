package traits.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class ScenarioLogger {

   Logger            logger;
   FileHandler       handler;
   SimpleFormatter   format;
   List<LogListener> logListeners = Collections.synchronizedList(new ArrayList<LogListener>());

   /**
    * @param logpath
    */
   public ScenarioLogger(String logpath) {
      try {
         logger = Logger.getLogger("Traits");
         handler = new FileHandler(logpath + "/traits.%u.%g.log", true);
         logger.setLevel(Level.ALL);
         format = new SimpleFormatter();
         handler.setFormatter(format);
      } catch (SecurityException | IOException e) {
         e.printStackTrace();
      }
   }

   /**
    * 
    * @param level
    * @param msg
    * @param thrown
    */
   public void log(Level level, String msg, Throwable thrown) {
      try {
         logger.log(level, msg, thrown);
         for (LogListener l : logListeners) {
            l.eavesdrop(level, msg);
         }
      } catch (Exception e) {
         logger.severe(e.getMessage());
      }
   }

   /**
    * @param l
    */
   public void registerLogListener(LogListener l) {
      logListeners.add(l);
   }
}
