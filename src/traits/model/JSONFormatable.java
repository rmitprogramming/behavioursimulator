/**
 * 
 */
package traits.model;

/**
 * @author anthonyrawlins
 *
 */
@SuppressWarnings("javadoc")
public interface JSONFormatable {
   public String exportAsJSON();
}
