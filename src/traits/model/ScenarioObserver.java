package traits.model;

/**
 * @author anthonyrawlins
 */
public interface ScenarioObserver {
   /**
    * @param e
    */
   public void handleScenarioEvent(ScenarioEvent e);
}
