package traits.model;

/**
 * Represents all the possible Scenario-wide events that affect the Simulation.
 * 
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */

public class ScenarioEvent {

   /**
    * @author anthonyrawlins
    */
   public enum EventType {

      /**
       * 
       */
      GETBUSY,

      /**
               * 
               */
      FRAME_READY,

      /**
                   * 
                   */
      PREGNANCY,

      /**
                 * 
                 */
      BIRTH,

      /**
             * 
             */
      DEATH,

      /**
             * 
             */
      NOTICE,

      /**
              * 
              */
      COMPLETE,

      /**
                * 
                */
      UPDATE,

      /**
              * 
              */
      READY,

      /**
             * 
             */
      GENOCIDE,

      /**
                * 
                */
      EPOCH_CHANGE,

      /**
                    * 
                    */
      RESOURCE_DEPLETED,

      /**
                         * 
                         */
      CONFLICT,

      /**
                * 
                */
      CONSUMED,
   }

   /**
    * 
    */
   public EventType type;
   private String   message;

   /**
    * 
    * @param type
    * @param msg
    */
   public ScenarioEvent(EventType type, String msg) {
      this.type = type;
      this.message = msg;
   }

   /**
    * 
    * @return the message
    */
   public String getMessage() {
      return message;
   }
}
