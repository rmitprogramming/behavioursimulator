package traits.model;

import java.util.logging.Level;

public interface LogListener {
   public void eavesdrop(Level lvl, String message);
}
