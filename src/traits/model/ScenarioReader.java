/**
 * 
 */
package traits.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.IllegalFormatException;
import java.util.Set;

import com.cedarsoftware.util.io.JsonReader;

import traits.Engine;
import traits.model.core.Gene;
import traits.model.core.GeneTag;
import traits.model.environment.Environment;
import traits.model.people.Harvester;
import traits.model.people.primitives.AbstractAgent;

/**
 * The scenario Reader creates the Scenario based on parameters read in from the
 * runfile.
 * 
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class ScenarioReader {

   private enum DebugCase {
      WRITE_TEST, RUN_TEST, READ_TEST
   }

   DebugCase dbgcase;

   private JsonReader      reader;
   private static Scenario s;

   private String inputFolder;

   private FileInputStream scene;
   private FileInputStream environment;
   private FileInputStream agents;

   /**
    * @param inputFolder
    * @since 0.2
    */
   public ScenarioReader(String inputFolder) {
      this.inputFolder = inputFolder;

      try {
         scene = new FileInputStream(inputFolder + "/scene.json");
      } catch (FileNotFoundException e2) {
         // TODO Auto-generated catch block
         e2.printStackTrace();
      }
      try {
         environment = new FileInputStream(inputFolder + "/environment.json");
      } catch (FileNotFoundException e1) {
         // TODO Auto-generated catch block
         e1.printStackTrace();
      }
      try {
         agents = new FileInputStream(inputFolder + "/agents.json");
      } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   /**
    * @return Scenario
    * @throws FileNotFoundException
    * @throws IllegalFormatException
    */
   public Scenario parseInputFiles() throws FileNotFoundException, IllegalFormatException {

      boolean DEBUG = true;
      if (!DEBUG) {
         //
         // // Read runfile...
         // JsonObject j;
         // try {
         // reader = Json.createReader(new FileReader(f));
         // j = (JsonObject) reader.read();
         // navigateTree(j, "Scenario");
         // s.model = j;
         // } catch (FileNotFoundException | IllegalFormatException e) {
         // e.printStackTrace();
         // }

      } else {

         dbgcase = DebugCase.WRITE_TEST;
         // dbgcase = DebugCase.READ_TEST;

         switch (dbgcase) {

         case WRITE_TEST:

            s = new Scenario();
            // Scenario.getEnvironment().setImage("src/wetlands.png");

            // Environment.setUseNoiseSeed(false);
            // Environment.setLevel_of_detail(5);
            // Environment.setThreshold(3);
            // Environment.setNoise1Scale(0.013f); // Recommended values!
            // Environment.setNoise2Scale(0.004f); // " "
            // Environment.setNoiseSeed(123);

            Scenario.getEnvironment().setWidth(550);
            Scenario.getEnvironment().setHeight(550);

            s.setEpochs(3000);

            // Add some test subjects...
            for (int i = 0; i < 30; i++) {
               Harvester h = new Harvester(Scenario.getEnvironment().randomLocation());

               h.setAge((int) Engine.appWrapper.random(16) + 16);

               // Perception MUST be an ODD Number
               h.add(new Gene(GeneTag.PERCEPTION, Gene.Type.MAX, 13));
               h.add(new Gene(GeneTag.SOCIABILITY, Gene.Type.MAX, 65));
               h.add(new Gene(GeneTag.PERSONAL_SPACE, Gene.Type.MAX, 10));

               // Energy
               h.add(new Gene(GeneTag.BASE_HEALTH, Gene.Type.ADDITIVE, 3200));
               h.add(new Gene(GeneTag.LIFESPAN, Gene.Type.MAX, 100));
               h.add(new Gene(GeneTag.CALORIE_COST_PER_PIXEL, Gene.Type.MAX, 15));
               h.add(new Gene(GeneTag.DAILY_CALORIE_COSTS, Gene.Type.MAX, 127));

               // Reproduction
               h.add(new Gene(GeneTag.SEXUAL_MATURITY, Gene.Type.MIN, 16));
               h.add(new Gene(GeneTag.MENOPAUSE, Gene.Type.MAX, 60));
               h.add(new Gene(GeneTag.CHANCE_OF_CONCEPTION, Gene.Type.MIN, 160));
               h.add(new Gene(GeneTag.MATING_DISTANCE, Gene.Type.MIN, 6));

               s.addAgent(h);
            }
            break;

         case READ_TEST:
            try {
               JsonReader sceneReader = new JsonReader(scene);
               s = (Scenario) sceneReader.readObject();
            } catch (Exception e) {
               e.printStackTrace();
            }

            try {
               JsonReader environmentReader = new JsonReader(environment);
               s.setEnvironment((Environment) environmentReader.readObject());
            } catch (Exception e) {

            }
            try {
               JsonReader agentReader = new JsonReader(agents);

               for (AbstractAgent a : (Set<AbstractAgent>) agentReader.readObject()) {
                  s.addAgent(a);
               }
            } catch (Exception e) {
               e.printStackTrace();
            }
            break;

         case RUN_TEST:
            break;
         }

      }
      return s;
   }
}
