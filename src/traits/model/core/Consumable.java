/**
 * 
 */
package traits.model.core;

/**
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public interface Consumable {
   /**
    * The entity consumes this Consumable in the implemented way.
    * 
    * @param by
    * @return boolean
    */
   boolean consume();
}
