package traits.model.core;

import processing.core.PVector;

/**
 * The Drawable interface provides the common methods required to prepare and
 * then render each Drawable object to the screen. render() is only used in GUI
 * mode.
 * 
 * @author Anthony Rawlins
 */
public interface Drawable extends Renders {

   /**
    * 
    * @return location as vector
    */
   public PVector getLocation();

   /**
    * 
    * @return velocity as vector
    */
   public PVector getVelocity();

}
