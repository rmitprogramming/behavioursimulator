package traits.model.core;

/**
 * A Gene is a message carried by an entity (in a collection)
 * <p>
 * A Gene is a number of values that mutate the base parameters (defaults) of an
 * Entity. Genes accumulate. If one Gene increases health by +50 and another
 * increases it by +100, then the Entity has +150 health above normal levels.
 * <p>
 * A Gene is a value mutation.
 * <p>
 * For example if an Entity has a base health value of 10000 but carries a Gene
 * that increases health by +1000, then offspring of that Entity will inherit
 * its Genes and therefore the health increase is passed to the offspring.
 * 
 * @author anthonyrawlins
 */
public class Gene implements Comparable<Gene> {

   private GeneTag tag;
   private Float   val;
   private Type    type;

   /**
    * The Type of Gene denotes how that gene will be passed to offspring.
    * 
    * @author anthonyrawlins
    */
   public enum Type {
      /**
       * Two additive genes result in a single gene with their values added
       */
      ADDITIVE,

      /**
       * Two subtractive genes result in a single gene with their values
       * subtracted. The smaller being subtracted from the larger.
       */
      SUBTRACTIVE,

      /**
       * Multiple genes result in a single gene with the average of their
       * values.
       */
      AVERAGE,

      /**
       * This gene (an exact copy) gets passed to the offspring, eliminating
       * like kinds.
       */
      DOMINANT,

      /**
       * This gene does not get passed on at all.
       */
      REGRESSIVE,

      /**
       * The gene (an exact copy) with the smallest value gets passed on.
       */
      MIN,

      /**
       * The gene (an exact copy) with the largest value gets passed on.
       */
      MAX,

      /**
       * Any one of this kind of gene can be passed on, but only one and its
       * chosen randomly.
       */
      RANDOM,

      /**
       * All copies of this gene are passed on. Multiple genes with the same key
       * are allowed to exist in the genome. And thus eliminates Set or Map
       * Collections as suitable containers for the genome.
       */
      STACKING,

      /**
       * This gene type only gets passed on if only one of the parents has it in
       * their genome. If both have it, it doesn't get passed on.
       */
      SINGLE
   }

   /**
    * Standard constructor
    * 
    * @param tag
    * @param t
    * @param val
    */
   public Gene(GeneTag tag, Gene.Type t, float val) {
      this.tag = tag;
      this.type = t;
      this.val = val;
   }

   /**
    * Copy constructor
    * 
    * @param g
    */
   public Gene(Gene g) {
      type = g.type;
      val = g.val;
      tag = g.tag;
   }

   /**
    * Given two Genes calculate the new Gene based on its Type OR... return null
    * if the Gene is STACKING ie., multiple copies of that Gene are allowed in a
    * genome. A dominant Gene will be passed down even if the value is inferior.
    * A regressive Gene won't be passed down even if its value is superior.
    * Dominant and regressive Genes are agnostic towards the type of another
    * Gene with the same key, so can be said to be stacking.
    * 
    * @param a
    * @param b
    * @return a new Gene or null
    */
   public static Gene trait(Gene a, Gene b) {
      float v;
      // We can only compare alike Genes
      if (a.getTag().equals(b.getTag())) {

         if (a.getType() == Gene.Type.DOMINANT || b.getType() == Gene.Type.REGRESSIVE) {
            return new Gene(a);

         } else if (b.getType() == Gene.Type.DOMINANT || a.getType() == Gene.Type.REGRESSIVE) {
            return new Gene(b);

         } else if (a.getType() == b.getType()) {
            switch (a.getType()) {

            case ADDITIVE:
               v = a.val + b.val;
               break;

            case SUBTRACTIVE:
               v = (a.val < b.val) ? b.val - a.val : a.val - b.val;
               break;

            case AVERAGE:
               v = (a.val + b.val) / 2;
               break;

            case MIN:
               v = Math.min(a.val, b.val);
               break;

            case MAX:
               v = Math.max(a.val, b.val);
               break;

            case RANDOM:
               v = (Math.random() > 0.5) ? a.val : b.val;
               break;

            case SINGLE:
               return new Gene(a);

            case STACKING:

            default:
               return null;
            }
            return new Gene(a.getTag(), a.type, v);
         }
      }
      return null;
   }

   // TODO JSONFormattable Interface instead of this...
   @Override
   public String toString() {
      return String.format("{\"Gene\": {\"%s\": \"%.1f\"}}", GeneTagLibrary.getStringValueForTag(this.tag), val);
   }

   /**
    * @return float
    */
   public float getVal() {
      return val;
   }

   /**
    * @return Gene.Type
    */
   public Gene.Type getType() {
      return type;
   }

   @Override
   public int compareTo(Gene o) {
      if (!this.tag.equals(o.getTag()))
         return -1;
      if (this.type != o.getType())
         return -1;
      return (int) (this.val - o.getVal());
   }

   /**
    * 
    * @param tag
    * @return float
    */
   public float getValueFor(GeneTag tag) {
      return (this.tag == tag) ? getVal() : 0;
   }

   /**
    * @return the tag
    */
   public GeneTag getTag() {
      return tag;
   }

   /**
    * @param tag
    *           the tag to set
    */
   public void setTag(GeneTag tag) {
      this.tag = tag;
   }

   /**
    * @param val
    *           the val to set
    */
   public void setVal(Float val) {
      this.val = val;
   }

   /**
    * @param type
    *           the type to set
    */
   public void setType(Type type) {
      this.type = type;
   }
}
