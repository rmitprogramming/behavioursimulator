package traits.model.core;

import java.util.List;

import traits.model.people.primitives.ReproducingEntity;

@SuppressWarnings("javadoc")
public interface Genetics {

   public List<? extends Gene> getGenes();

   public void inheritGenes(List<Gene> genes);

   public void add(Gene g);

   public float getGenomicVarianceFor(GeneTag tag);

   public void impregnateBy(ReproducingEntity partner);

   public void giveBirth();

   public int geneticHealth();

}
