package traits.model.core;

@SuppressWarnings("javadoc")
public enum GeneTag {
   AGE, BASE_HEALTH, MAX_SPEED, LIFESPAN, PERCEPTION, DAILY_CALORIE_COSTS, CALORIE_COST_PER_PIXEL, SEXUAL_MATURITY, GESTATION_PERIOD, CHANCE_OF_CONCEPTION, MATING_DISTANCE, MENOPAUSE, ENERGY_REQUIRED_FOR_BIRTH, SOCIABILITY, PERSONAL_SPACE, STARVATION_THRESHOLD, HUNGER_THRESHOLD
}
