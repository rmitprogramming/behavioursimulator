package traits.model.core;

import traits.model.people.primitives.ReproducingEntity;

/**
 * @author anthonyrawlins
 */
public class Lineage {

   private Lineage   father;
   private Lineage   mother;
   ReproducingEntity offspring;

   /**
    * @param me
    */
   public Lineage(ReproducingEntity me) {
      offspring = me;
      father = null;
      mother = null;
   }

   /**
    * @author anthonyrawlins
    * @since 0.3
    * @param fathersLineage
    * @param mothersLineage
    */
   public void add(Lineage fathersLineage, Lineage mothersLineage) {
      father = fathersLineage;
      mother = mothersLineage;
   }

   /**
    * @return the mother
    */
   public Lineage getMothers() {
      return mother;
   }

   /**
    * @return the fathers line
    */
   public Lineage getFathers() {
      return father;
   }

   /**
    * @return the mothers name
    */
   public String mothersName() {
      return (mother == null) ? "" : mother.getName();
   }

   /**
    * @return the fathers name
    */
   public String fathersName() {
      return (father == null) ? "" : father.getName();
   }

   /**
    * @return the name of the offspring
    */
   public String getName() {
      return offspring.getName();
   }

   /**
    * Search the Tree
    * 
    * @param s
    * @return if the name is in the family tree
    */
   public boolean isFamily(ReproducingEntity s) {

      if (s.getName() == offspring.getName())
         return true;

      if (mother == null && father == null) {
         return false;
      } else if (mother == null) {
         return father.isFamily(s);
      } else if (father == null) {
         return mother.isFamily(s);
      } else
         return (mother.isFamily(s) || father.isFamily(s));
   }

}