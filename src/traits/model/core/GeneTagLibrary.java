package traits.model.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Powered by internal bi-directional map.
 * 
 * @author anthonyrawlins
 *
 */

public class GeneTagLibrary {

   private static Map<GeneTag, String> keymap;
   private static Map<String, GeneTag> valmap;

   public GeneTagLibrary() {
      keymap = Collections.synchronizedMap(new HashMap<GeneTag, String>());
      keymap.put(GeneTag.BASE_HEALTH, "age");
      keymap.put(GeneTag.BASE_HEALTH, "base health");
      keymap.put(GeneTag.CALORIE_COST_PER_PIXEL, "calorie cost per pixel");
      keymap.put(GeneTag.CHANCE_OF_CONCEPTION, "chance of conception");
      keymap.put(GeneTag.DAILY_CALORIE_COSTS, "daily calorie costs");
      keymap.put(GeneTag.ENERGY_REQUIRED_FOR_BIRTH, "energy required for birth");
      keymap.put(GeneTag.GESTATION_PERIOD, "bgestation period");
      keymap.put(GeneTag.HUNGER_THRESHOLD, "hunger threshold");
      keymap.put(GeneTag.LIFESPAN, "lifespan");
      keymap.put(GeneTag.MATING_DISTANCE, "mating distance");
      keymap.put(GeneTag.MAX_SPEED, "max speed");
      keymap.put(GeneTag.MENOPAUSE, "menopause");
      keymap.put(GeneTag.PERCEPTION, "perception");
      keymap.put(GeneTag.PERSONAL_SPACE, "personal space");
      keymap.put(GeneTag.SEXUAL_MATURITY, "sexual maturity");
      keymap.put(GeneTag.SOCIABILITY, "sociability");
      keymap.put(GeneTag.STARVATION_THRESHOLD, "starvation threshold");

      valmap = Collections.synchronizedMap(new HashMap<String, GeneTag>());
      for (Map.Entry<GeneTag, String> entry : keymap.entrySet()) {
         valmap.put(entry.getValue(), entry.getKey());
      }
   }

   public static String getStringValueForTag(GeneTag g) throws IllegalArgumentException {
      if (!keymap.containsKey(g))
         throw new IllegalArgumentException();
      return keymap.get(g);
   }

   public static GeneTag getTagForString(String s) throws IllegalArgumentException {
      if (!valmap.containsKey(s))
         throw new IllegalArgumentException();
      return valmap.get(s);
   }
}
