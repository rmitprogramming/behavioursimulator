/**
 * 
 */
package traits.model.core;

/**
 * This is a placeholder for further Tag functionality.
 * 
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class Tag {

   String name;

   /**
    * @param t
    */
   public Tag(String t) {
      name = t;
   }

}
