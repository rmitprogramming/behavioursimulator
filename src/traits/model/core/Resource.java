/**
 * 
 */
package traits.model.core;

import processing.core.PVector;
import traits.Engine;
import traits.model.Scenario;
import traits.model.ScenarioEvent;
import traits.model.ScenarioEvent.EventType;

/**
 * A generic resource that when 'consumed' replenishes health (Calories).
 * Represents an amount and quality of resource and a range from its origin
 * where the resource can be reached. Think a 'forest' or 'savanah' with
 * scattered resources within that area. As long as the Consumer is within range
 * of the Resources origin, they are said to be able to consume the resource.
 * 
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class Resource extends Inanimate implements Consumable {

   private static final boolean replenishment        = true;
   private static final float   defaultResourceScale = 20.0f;
   private int                  unitvalue;                   // Calories

   private int        amount;
   private static int INITIAL_AMOUNT = 30;
   private static int MAX_AMOUNT     = 50;
   private static int MAX_QUALITY    = 1250;
   // Calories per meal
   // (should be approx 2x
   // dailyIntake
   // Requirements to
   // balance out)
   private float      growth_rate    = 1.02f;
   // Randomly skews
   // towards +2%

   /**
    * @param x
    * @param y
    */
   public Resource(float x, float y) {
      super(x, y);

      // Half to full quality
      this.unitvalue = (int) parent.random(MAX_QUALITY / 2) + (MAX_QUALITY / 2);
      this.amount = INITIAL_AMOUNT;
      this.name = "Generic Food";
   }

   /**
    * @param loc
    */
   public Resource(PVector loc) {
      super(loc.x, loc.y);
      this.unitvalue = (int) parent.random(MAX_QUALITY / 2) + MAX_QUALITY / 2;
      this.amount = INITIAL_AMOUNT;
      this.name = "Generic Food";
   }

   @Override
   public boolean consume() {
      if (getAmount() > 0) {
         --amount;
         return true;
      } else {
         notifyObservers(
               new ScenarioEvent(EventType.RESOURCE_DEPLETED, String.format("%s has been depleted.", this.toString())));
         respawn();
      }
      return false;
   }

   /**
    * recycles the resource by resetting its location, amount and quality to new
    * values.
    */
   public void respawn() {
      setX(Engine.appWrapper.random(Scenario.getEnvironment().randomUnobstructedLocation().x));
      setY(Engine.appWrapper.random(Scenario.getEnvironment().randomUnobstructedLocation().y));
      this.amount = INITIAL_AMOUNT;
      this.unitvalue = (int) parent.random(MAX_QUALITY / 2) + MAX_QUALITY / 2;
   }

   @Override
   public void behave() {
      // Natural resource growth
      if (amount <= 0) {
         notifyObservers(
               new ScenarioEvent(EventType.RESOURCE_DEPLETED, String.format("%s has been depleted.", this.toString())));
         respawn();

      } else if (replenishment) {
         if (amount < INITIAL_AMOUNT && (Engine.getScenario().getCurrentEpoch() % 2 == 0)) {
            amount += (int) (parent.random(1) * growth_rate);
         }
      }
      if (amount > (0.45 * MAX_AMOUNT) && unitvalue < MAX_QUALITY) {
         // quality improves up to limit.
         unitvalue += (int) (parent.random(1) * growth_rate);
      }

   }

   @Override
   public void render() {
      tags.clear();
      tags.add(String.format("A:%d, Q:%d", getAmount(), getQuality()));
      // showTags();

      if (amount > 0) {
         // Defaults to a small green circle
         parent.pushMatrix();
         if (Engine.is3DAccelerated()) {
            parent.translate(getX(), getY(), 5);
         } else {
            parent.translate(getX(), getY());
         }

         parent.strokeWeight(0.5f);
         parent.stroke(42, 88, 24, 60);

         // Amount & Quality - Richer the better

         /*
          * poor quality is rgb(46, 117, 34) good quality is rgb(20, 49, 14)
          * 
          * amount is scale or radii
          */
         float quality = ((float) unitvalue / MAX_QUALITY);

         int rbit = (int) (46 - 20 * quality);
         int gbit = (int) (117 - 49 * quality);
         int bbit = (int) (34 - 14 * quality);

         parent.fill(parent.color(rbit, gbit, bbit), (int) ((float) amount / (float) MAX_AMOUNT * 90.0f));

         parent.ellipse(0, 0, radii() * 2, radii() * 2);
         if (Engine.is3DAccelerated())
            parent.line(0, 0, 0, 0, 0, 30.0f);
         parent.popMatrix();
      }
   }

   /**
    * @return float
    */
   public float radii() {
      // Normal size for Resources is 30.0f
      return (float) amount / MAX_AMOUNT * defaultResourceScale;

   }

   /**
    * @return boolean
    */
   public boolean depleted() {
      return (amount > 0) ? false : true;
   }

   /**
    * @return int
    */
   public int getQuality() {
      return unitvalue;
   }

   /**
    * @return int
    */
   public int getAmount() {
      return amount;
   }

   @Override
   public String toString() {
      return "{\"Generic Food\": [\"Quality\": " + unitvalue + ", \"Amount\": " + amount + "]}";
   }
}
