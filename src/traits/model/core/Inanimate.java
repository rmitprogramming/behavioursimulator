/**
 * 
 */
package traits.model.core;

import traits.model.people.primitives.AbstractAgent;

/**
 * This is the base class for non-moving objects that affect the Scenario. It
 * has no functionality except what it inherits from Agent.
 * 
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public abstract class Inanimate extends AbstractAgent {

   /**
    * @param y
    * @param x
    */
   public Inanimate(float x, float y) {
      super(x, y);
   }

}
