package traits.model.core;

/**
 * @author anthonyrawlins
 */
public interface Renders {

   /**
    * Does the actual drawing of this object to screen.
    */
   public void render();

   /**
    * Gets the x-coordinate as a floating point value
    * 
    * @return float
    */
   public float getX();

   /**
    * Gets the y-coordinate as a floating point value
    * 
    * @return float
    */
   public float getY();
}
