package traits.model.core;

@SuppressWarnings("javadoc")
public interface Seeker {
   public Resource nearestResource();

   public void seekResource();
}
