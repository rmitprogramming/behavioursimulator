package traits.model.environment;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;

/**
 * @author anthonyrawlins
 */
public class ReplenishingEnvironment extends EdibleEnvironment {

   private PImage slightlyOriginal;

   /**
    * @param w
    * @param h
    * @throws IllegalArgumentException
    */
   public ReplenishingEnvironment(int w, int h) throws IllegalArgumentException {
      super(w, h);
   }

   public ReplenishingEnvironment(int i, int j, PApplet pApplet) {
      super(i, j, pApplet);
   }

   public void setImage(String filepath) {
      super.setImage(filepath);
      slightlyOriginal = parent.createImage(img.width, img.height, PImage.ARGB);

      img.loadPixels();
      slightlyOriginal.copy(img, 0, 0, w, h, 0, 0, w, h);
      img.updatePixels();

      for (int i = 0; i < slightlyOriginal.pixels.length; i++) {
         int pixelColor = slightlyOriginal.pixels[i];

         // Faster way of getting (argb)
         int a = 3 << 24;
         int r = (pixelColor >> 16) & 0xFF;
         int g = (pixelColor >> 8) & 0xFF;
         int b = pixelColor & 0xFF;

         a = 3 << 24; // We want a really low alpha!
         r = r << 16;
         g = g << 8;
         b = b << 0;

         // Equivalent to "color argb = color(r, g, b, a)" but faster!
         slightlyOriginal.pixels[i] = a | r | g | b;
      }

      slightlyOriginal.updatePixels();
   }

   @Override
   public void render() {
      if (slightlyOriginal != null) {
         img.loadPixels();
         img.blend(slightlyOriginal, 0, 0, w, h, 0, 0, w, h, PConstants.LIGHTEST);
         img.updatePixels();
      }
      super.render();
   }
}
