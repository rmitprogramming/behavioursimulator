package traits.model.environment;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

/**
 * @author anthonyrawlins
 */
public class EdibleEnvironment extends TrailsEnvironment {

   private static final boolean drawFoodMap = false;
   private PImage               foodmap;

   /**
    * @param w
    * @param h
    * @throws IllegalArgumentException
    */
   public EdibleEnvironment(int w, int h) throws IllegalArgumentException {
      super(w, h);
   }

   public EdibleEnvironment(int i, int j, PApplet pApplet) {
      super(i, j, pApplet);
   }

   @Override
   public void setImage(String filepath) {
      super.setImage(filepath);
      generateFoodMap();
   }

   @Override
   public void setWidth(int w) {
      super.setWidth(w);
      generateFoodMap();
   }

   public void setHeight(int h) {
      super.setHeight(h);
      generateFoodMap();
   }

   private void generateFoodMap() {
      foodmap = parent.createImage(w, h, PConstants.ARGB);
      foodmap.loadPixels();
      foodmap.copy(img, 0, 0, w, h, 0, 0, w, h);
      foodmap.filter(PConstants.BLUR, 0.5f);
      foodmap.updatePixels();
   }

   /**
    * We call BOTH blazeTrail() and eatBackground() here to allow for
    * environments tha might use different images for food distribution and the
    * visible background.
    * 
    * @param x
    * @param y
    * @return int as calories
    */
   public int eatPixel(float x, float y) {

      blazeTrail(x, y); // Visible

      int pixelColor = eatBackground(foodmap, x, y); // Invisible but important!

      int calorieValue = (pixelColor >> 8) & 0xFF; // ie., how much green 0-255

      return calorieValue * 20;
   }

   /**
    * @param location
    * @return PVector to best food source
    */
   public PVector getGreenestPixelIn5x5Near(PVector location) {

      int hx = (int) Math.floor(location.x);
      int hy = (int) Math.floor(location.y);

      int newpos_x = 0;
      int newpos_y = 0;

      int offset_gx = 0;
      int offset_gy = 0;

      // Edge detection and bounds checking
      /*
       * ooooo... ooooo... oo•oo... < hy = 2 ooooo... ooooo... ........
       * 
       * ^ hx = 2
       * 
       * 
       * xxxxx... •xxxx... < get this grid, don't go out of bounds! xx0oo... <
       * offset_gy = +1; xxooo... xxooo... ........ --^ offset_gx = +2;
       * 
       * 
       */

      // get a sub-image of the surrounding 5x5 pixel grid

      if (hx < 3)
         offset_gx = 3 - hx; // in the above example, 3-hx = 3-1 = 2 =
      // offset_gx
      if (hy < 3)
         offset_gy = 3 - hy; // in the above example, 3-hy = 3-2 = 1 =
      // offset_gy

      /*
       * ........ ...oooxx ...oooxx ...oo0xx < offset_gy = -2; ...xxxxx |
       * ...xxxx• | _____^-- offset_gx = -2;
       * 
       * 
       */

      if (hx > w - 3)
         offset_gx = 0 - (w - hx + 2);
      if (hy > h - 3)
         offset_gx = 0 - (h - hy + 2);

      int subx = hx - 2 + offset_gx;
      int suby = hy - 2 + offset_gy;

      PImage subimg = foodmap.get(subx, suby, 5, 5);

      subimg.loadPixels();

      int calorieValue = 0;
      int bestCalorieValue = 0;
      int which = -1;

      // Get the pixel in this grid with the highest calorific value
      for (int p = 0; p < subimg.pixels.length; p++) {
         calorieValue = (subimg.pixels[p] >> 8) & 0xFF; // green value

         if (calorieValue > bestCalorieValue) {
            bestCalorieValue = calorieValue;
            which = p;
         }

      }

      /*
       * eg., p=15
       * 
       * ..01234 0 ooooo 1 ooooo 2 ooxoo 3 •oooo 4 ooooo
       * 
       */
      if (which != -1) {
         newpos_x = which % 5;
         newpos_y = (int) Math.floor(which / 5);
      }
      PVector result = new PVector(newpos_x + offset_gx - 2, newpos_y + offset_gy - 2);
      return result;
   }

   @Override
   public void render() {
      super.render();
      if (drawFoodMap)
         parent.image(foodmap, 0, 0);
   }
}
