package traits.model.environment;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;
import traits.Engine;
import traits.model.ScenarioEvent;
import traits.model.ScenarioObserver;
import traits.model.core.Renders;

/**
 * @author anthonyrawlins
 */
public class SimpleEnvironment implements ScenarioObserver, Renders {

   protected PApplet parent;
   protected int     w;
   protected int     h;
   protected PImage  img;
   protected PImage  original;

   /**
    * Constructor that creates a random environment
    * 
    * @param w
    * @param h
    * @throws IllegalArgumentException
    */
   public SimpleEnvironment(int w, int h) throws IllegalArgumentException {
      if (w <= 0)
         throw new IllegalArgumentException();
      if (h <= 0)
         throw new IllegalArgumentException();
      this.w = w;
      this.h = h;
      this.parent = Engine.appWrapper;
   }

   public SimpleEnvironment(int w, int h, PApplet parent) throws IllegalArgumentException {
      if (w <= 0)
         throw new IllegalArgumentException();
      if (h <= 0)
         throw new IllegalArgumentException();
      this.w = w;
      this.h = h;
      this.parent = parent;
   }

   /**
    * Constructor that uses an Image as its source
    * 
    * @param filepath
    */
   public void setImage(String filepath) {

      img = parent.loadImage(filepath);
      img.loadPixels();

      original = parent.createImage(img.width, img.height, PImage.ARGB);
      original.copy(img, 0, 0, w, h, 0, 0, w, h);
      original.updatePixels();
      img.updatePixels();

      this.w = img.width;
      this.h = img.height;
   }

   /**
    * @param w
    * @param h
    * @return PImage
    */
   private PImage generateImage(int w, int h) {

      PImage img = parent.createImage(this.w, this.h, PImage.RGB);

      float noiseScale = 0.02f;
      parent.noiseDetail(5, 0.5f);
      // Various greens
      for (int i = 0; i < h; i++) {
         for (int j = 0; j < w; j++) {
            float noiseValue = parent.noise(i * noiseScale, j * noiseScale);
            img.pixels[i * w + j] = parent.color(43 + parent.random(14 * noiseValue),
                  22 + parent.random(137 * noiseValue), 40 + parent.random(30 * noiseValue));
         }
      }

      // // Try to quickly represent grass
      // img.filter(PConstants.BLUR, 4);
      img.filter(PConstants.DILATE);

      // Hmmm happy with that
      img.updatePixels();

      return img;
   }

   /**
    * @return width
    */
   public int getWidth() {
      return w;
   }

   /**
    * @return height
    */
   public int getHeight() {
      return h;
   }

   @Override
   public void render() {
      if (img == null) {
         img = generateImage(w, h);
      } else
         parent.image(img, 0, 0);
   }

   /**
    * @param w
    */
   public void setWidth(int w) {
      this.w = w;
      if (img == null || (img.width != w))
         img = generateImage(w, h);
   }

   /**
    * @param h
    */
   public void setHeight(int h) {
      this.h = h;
      if (img == null || (img.height != h))
         img = generateImage(w, h);
   }

   @Override
   public float getX() {
      return 0;
   }

   @Override
   public float getY() {
      return 0;
   }

   /**
    * @return PVector
    */
   public PVector randomUnobstructedLocation() {
      return new PVector(parent.random(getWidth()), parent.random(getHeight()));
   }

   @Override
   public void handleScenarioEvent(ScenarioEvent e) {
   }
}
