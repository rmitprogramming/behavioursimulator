package traits.model.environment;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

/**
 * ObstructedEnvironment uses Perlin Noise create a dynamic environment where
 * some grid coordinates are 'obstructed' or impassable to ObstructionAware
 * agents. Any grid coordinate below the 'threshold' value is said to be
 * impassable. By default the Perlin Noise creates a random 2D noise space every
 * time the project is launched. By passing a Noise Seed number the noise will
 * be created the same way resulting in the same map for multiple iterations. We
 * leave the base class Agents able to move freely to allow for possible future
 * flying classes like birds or planes.
 * 
 * @author anthonyrawlins
 */
public class ObstructedEnvironment extends SimpleEnvironment implements ObstructionObserver {

   private static final boolean showObstructions = false;
   private static boolean       useNoiseSeed     = false;
   private static int           noiseSeed        = 0;
   private static float         noise1Scale      = 0.013f;
   private static float         noise2Scale      = 0.003f;// 0.005-0.03 is best
   private static int           level_of_detail  = 6;
   private static float         falloff          = 0.85f;
   private static int           threshold        = 128;   // 0-255

   private static PImage obstructionMap;

   /**
    * @param w
    * @param h
    */

   public ObstructedEnvironment(int w, int h) {
      super(w, h);
   }

   public ObstructedEnvironment(int i, int j, PApplet pApplet) {
      super(i, j, pApplet);
   }

   /**
    * @param noise1Scale
    *           the noise1Scale to set
    */
   public static void setNoise1Scale(float noise1Scale) {
      ObstructedEnvironment.noise1Scale = noise1Scale;
   }

   /**
    * @param noise2Scale
    *           the noise2Scale to set
    */
   public static void setNoise2Scale(float noise2Scale) {
      ObstructedEnvironment.noise2Scale = noise2Scale;
   }

   /**
    * @param level_of_detail
    *           the level_of_detail to set
    */
   public static void setLevel_of_detail(int level_of_detail) {
      ObstructedEnvironment.level_of_detail = level_of_detail;
   }

   /**
    * @param falloff
    *           the falloff to set
    */
   public static void setFalloff(float falloff) {
      ObstructedEnvironment.falloff = falloff;
   }

   /**
    * @param threshold
    *           the threshold to set
    */
   public static void setThreshold(int threshold) {
      ObstructedEnvironment.threshold = threshold;
   }

   /**
    * @param useNoiseSeed
    *           the useNoiseSeed to set
    */
   public static void setUseNoiseSeed(boolean useNoiseSeed) {
      ObstructedEnvironment.useNoiseSeed = useNoiseSeed;
   }

   /**
    * Setting a noiseSeed will generate the SAME map each time the program is
    * run. Setting no noiseSeed, or setting useNoiseSeed to false will result in
    * a randomly generated map using Perlin Noise.
    * 
    * @param i
    */
   public static void setNoiseSeed(int i) {
      noiseSeed = i;
   }

   private PImage generateRandomObstructionMap(int w, int h) {

      if (useNoiseSeed)
         parent.noiseSeed(noiseSeed);
      parent.noiseDetail(level_of_detail, falloff);

      PImage grom = parent.createImage(this.w, this.h, PImage.ARGB);
      PImage mask = parent.createImage(this.w, this.h, PImage.RGB);

      final int obstructionColor = parent.color(66, 101, 19);

      for (int i = 0; i < h; i++) {
         for (int j = 0; j < w; j++) {
            int pixelValue = 255 - (int) (parent.noise(i * noise1Scale, j * noise1Scale) * 255);
            grom.pixels[i * w + j] = parent.color(obstructionColor, pixelValue);
         }
      }
      for (int i = 0; i < h; i++) {
         for (int j = 0; j < w; j++) {
            int pixelValue = 255 - (int) (parent.noise(i * noise2Scale, j * noise2Scale) * 255);
            mask.pixels[i * w + j] = parent.color(pixelValue);
         }
      }
      mask.filter(PConstants.THRESHOLD);
      mask.filter(PConstants.BLUR, 6);
      mask.filter(PConstants.THRESHOLD);
      mask.filter(PConstants.BLUR, 1);

      mask.updatePixels();

      grom.mask(mask);

      grom.updatePixels();
      return grom;
   }

   @Override
   public void render() {
      super.render();
      // Don't really need to show it. but for debugging...
      if (obstructionMap == null) {
         obstructionMap = generateRandomObstructionMap(w, h);
      }
      if (showObstructions)
         parent.image(obstructionMap, 0, 0);

   }

   @Override
   public void setWidth(int w) {
      super.setWidth(w);
      obstructionMap = generateRandomObstructionMap(w, h);
   }

   @Override
   public void setHeight(int h) {
      super.setHeight(h);
      obstructionMap = generateRandomObstructionMap(w, h);
   }

   public void setImage(String path) {
      super.setImage(path);
      setWidth(img.width);
      setHeight(img.height);
   }

   /**
    * @param location
    * @return boolean
    */
   public boolean isObstructed(PVector location) {
      return isObstructed(location.x, location.y);
   }

   /**
    * Uses colors lighter than a % of grey as passable, less than that is
    * impassable. Uses the constant 'threshold' value.
    * 
    * @param x
    * @param y
    * @return boolean
    */
   public boolean isObstructed(float x, float y) {
      return (parent.color(obstructionMap.get((int) x, (int) y)) < threshold) ? false : true;
   }

   /**
    * x,y coordinates wrapped in a PVector of a random location in the
    * Environment that is not obstructed.
    * 
    * @return PVector
    */
   public PVector randomUnobstructedLocation() {
      float x = 0;
      float y = 0;
      PVector r = new PVector(x, y);
      do {
         x = parent.random(w);
         y = parent.random(h);
         r.set(x, y);
      } while (isObstructed(r));

      return r;
   }
}
