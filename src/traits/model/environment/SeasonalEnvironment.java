package traits.model.environment;

import processing.core.PApplet;
import traits.Engine;
import traits.model.Scenario;
import traits.model.ScenarioEvent;
import traits.model.ScenarioObserver;

/**
 * @author anthonyrawlins
 */
public class SeasonalEnvironment extends ObstructedEnvironment implements ScenarioObserver {

   private static final boolean showSeasons = false;
   int                          day_of_year;
   Season                       season;

   static int daysPerSeason = (Scenario.epochsPerYear / 4);

   @SuppressWarnings("javadoc")
   public enum Season {
      SUMMER, AUTUMN, WINTER, SPRING
   }

   int autumn;
   int winter;
   int spring;
   int summer;
   int season_days = 0;

   /**
    * @param w
    * @param h
    */
   public SeasonalEnvironment(int w, int h) {
      super(w, h);
      season = Season.SPRING;

   }

   public SeasonalEnvironment(int i, int j, PApplet pApplet) {
      super(i, j, pApplet);
      season = Season.SPRING;
   }

   @Override
   public void render() {
      super.render();

      if (showSeasons) {
         if (season == Season.AUTUMN) {
            parent.fill(summer, daysPerSeason - season_days);
            parent.rect(0, 0, parent.width, parent.height);
            parent.fill(autumn, season_days);
            parent.rect(0, 0, parent.width, parent.height);
         } else if (season == Season.WINTER) {
            parent.fill(autumn, daysPerSeason - season_days);
            parent.rect(0, 0, parent.width, parent.height);
            parent.fill(winter, season_days);
            parent.rect(0, 0, parent.width, parent.height);
         } else if (season == Season.SPRING) {
            parent.fill(winter, daysPerSeason - season_days);
            parent.rect(0, 0, parent.width, parent.height);
            parent.fill(spring, season_days);
            parent.rect(0, 0, parent.width, parent.height);
         } else if (season == Season.SUMMER) {
            parent.fill(spring, daysPerSeason - season_days);
            parent.rect(0, 0, parent.width, parent.height);
            parent.fill(summer, season_days);
            parent.rect(0, 0, parent.width, parent.height);
         }
      }
   }

   @Override
   public void handleScenarioEvent(ScenarioEvent e) {

      autumn = parent.color(133, 68, 10);
      winter = parent.color(133, 122, 78);
      spring = parent.color(77, 156, 29);
      summer = parent.color(86, 133, 17);

      switch (e.type) {
      case UPDATE:
      case EPOCH_CHANGE:
         if ((Engine.getScenario().getCurrentEpoch() % daysPerSeason) == 0) {
            if (season == Season.SPRING) {
               season = Season.SUMMER;
            } else if (season == Season.SUMMER) {
               season = Season.AUTUMN;
            } else if (season == Season.AUTUMN) {
               season = Season.WINTER;
            } else if (season == Season.WINTER) {
               season = Season.SPRING;
            }
            season_days = 0;
         } else {
            season_days++;
         }
         break;
      default:
         break;
      }

   }
}
