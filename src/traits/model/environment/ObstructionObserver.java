package traits.model.environment;

import processing.core.PVector;


@SuppressWarnings("javadoc")
public interface ObstructionObserver {
   public boolean isObstructed(float x, float y);
   public boolean isObstructed(PVector location);
}
