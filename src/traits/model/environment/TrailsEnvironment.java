/**
 * 
 */
package traits.model.environment;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author anthonyrawlins
 */
public class TrailsEnvironment extends SeasonalEnvironment {

   /**
    * @param w
    * @param h
    * @throws IllegalArgumentException
    */
   public TrailsEnvironment(int w, int h) throws IllegalArgumentException {
      super(w, h);
   }

   public TrailsEnvironment(int i, int j, PApplet pApplet) {
      super(i, j, pApplet);
   }

   /**
    * @param x
    * @param y
    * @return int
    */
   public int blazeTrail(float x, float y) {
      return eatBackground(img, x, y);
   }

   /**
    * @param img
    * @param x
    * @param y
    * @return int
    */
   public int eatBackground(PImage img, float x, float y) {
      img.loadPixels();
      int pixelColor = (img.get((int) x, (int) y));

      int a = (pixelColor >> 24) & 0xFF;
      int r = (pixelColor >> 16) & 0xFF; // Faster way of getting red(argb)
      int g = (pixelColor >> 8) & 0xFF; // Faster way of getting green(argb)
      int b = pixelColor & 0xFF; // Faster way of getting blue(argb)

      r -= 32;
      if (r < 0)
         r = 0;
      g -= 32;
      if (g < 0)
         g = 0;
      b -= 32;
      if (b < 0)
         b = 0;

      img.set((int) x, (int) y, parent.color(r, g, b, a));
      img.updatePixels();

      return pixelColor;
   }
}
