/**
 * 
 */
package traits.model.environment;

import processing.core.PApplet;
import processing.core.PVector;

/**
 * @author anthonyrawlins
 */
public class Environment extends ReplenishingEnvironment {

   /**
    * @param w
    * @param h
    */
   public Environment(int w, int h) {
      super(w, h);
   }

   public Environment(int i, int j, PApplet pApplet) {
      super(i, j, pApplet);
   }

   /**
    * @return PVector
    */
   public PVector randomLocation() {
      return new PVector(parent.random(w), parent.random(h));
   }

   public void setEngine(PApplet environmentEditor) {
      parent = environmentEditor;
   }

}
