/**
 * 
 */
package traits.model.people.primitives;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import processing.core.PApplet;
import processing.core.PVector;
import traits.Engine;
import traits.model.Scenario;
import traits.model.ScenarioEvent;
import traits.model.ScenarioEvent.EventType;
import traits.model.core.Consumable;
import traits.model.core.Drawable;
import traits.model.core.Gene;
import traits.model.core.GeneTag;
import traits.model.core.Genetics;
import traits.model.core.Lineage;
import traits.model.environment.TrailsEnvironment;
import traits.model.people.FamilyTracking;
import traits.model.people.Harvester;

/**
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class ReproducingEntity extends AbstractAgent implements Mortal, Procreate, Genetics, FamilyTracking {

   protected final static int   calorieCostPerPixel    = 0;
   protected final static int   dailyCalorieCosts      = 0;
   protected final static int   chanceOfConception     = 10;
   protected final static int   gestationPeriod        = 240;
   protected final static int   energyRequiredForBirth = 2050;
   protected final static int   baseHealth             = 8500;                  // Fat
   protected final static int   maxHealth              = Integer.MAX_VALUE;     // Calories
   protected final static int   menopause              = 0;
   protected final static int   lifespan               = 0;
   protected final static int   sexualMaturity         = 0;
   private final static float   starvationThreshold    = 0.25f;
   private final static float   hungerThreshold        = 0.95f;
   private final static float   maxSteeringForce       = 0.06f;
   private final static float   maxSpeed               = (float) (Math.sqrt(8));
   private final static float   defaultPersonalSpace   = 5.55f;
   private final static float   defaultSociability     = 15.5f;
   protected final static float defaultMatingDistance  = 4.34f;                 // Maximum

   protected int              health;
   protected List<Gene>       genome;
   private Lineage            familyTree;
   private String             myMother      = null;
   private String             myFather      = null;
   private int                age;
   private boolean            alive         = true;
   private boolean            pregnant      = false;
   private int                days_pregnant = 0;
   private String             cause_of_death;
   private ReproducingEntity  parent2       = null;
   private Gender             gender;
   private float              sociability;
   private float              personalSpace;
   private static NameFactory nameFactory;

   @SuppressWarnings("javadoc")
   public enum Gender {
      Male, Female
   }

   /**
    * @param x
    * @param y
    * @since 0.3
    */
   public ReproducingEntity(float x, float y) {
      super(x, y);

      genome = Collections.synchronizedList(new ArrayList<Gene>());
      sociability = defaultSociability;
      personalSpace = defaultPersonalSpace;
      health = baseHealth;
      age = 0;
      setGender((parent.random(2) > 1) ? Gender.Male : Gender.Female);

      nameFactory = new NameFactory();

      if (this.getGender() == Gender.Female)
         this.setName(nameFactory.getFemaleName());
      if (this.getGender() == Gender.Male)
         this.setName(nameFactory.getMaleName());

      familyTree = new Lineage(this);

   }

   /**
    * @param loc
    */
   public ReproducingEntity(PVector loc) {
      this(loc.x, loc.y);
   }

   /**
    * @return float
    */
   public float getPersonalSpace() {
      return personalSpace + getGenomicVarianceFor(GeneTag.PERSONAL_SPACE);
   }

   /**
    * @return float
    */
   public float getSociability() {
      return sociability + getGenomicVarianceFor(GeneTag.SOCIABILITY);
   }

   /**
    * @see traits.model.people.primitives.Mortal#die(String)
    */
   @Override
   public void die(String cause_of_death) {
      this.tags.clear();
      this.tag(cause_of_death);
      alive = false;
      this.cause_of_death = cause_of_death;
      notifyObservers(new ScenarioEvent(EventType.DEATH,
            String.format("%s has died, aged %d, of %s.", this.toString(), this.getAge(), cause_of_death)));
      Scenario.getDeaths().add(this);
   }

   /**
    * Now updated for genetic variance
    * 
    * @since 0.3
    */
   @Override
   public void behave() {

      if (alive) {
         if ((getHealth() <= 0)) {
            die("Starvation");
         } else {
            Engine.getScenario();
            if (Engine.getScenario().getCurrentEpoch() % Scenario.epochsPerYear == 0) {
               birthday();
            } else {

               // Natural starvation
               setHealth(getHealth() - (dailyCalorieCosts + getGenomicVarianceFor(GeneTag.DAILY_CALORIE_COSTS)));

               cycle();

            }
         }
      }
   }

   private void setHealth(float f) {
      this.health = (int) f;
   }

   /**
    * Now updated for genetic variation
    * 
    * @since 0.3
    */
   private void cycle() {

      if (isStarving()) {
         tags.add("Starving");
      } else {
         tags.remove("Starving");
      }

      this.tags.remove("Gave birth!");

      if (pregnant) {

         int gp = gestationPeriod + (int) getGenomicVarianceFor(GeneTag.GESTATION_PERIOD);
         int be = energyRequiredForBirth + (int) getGenomicVarianceFor(GeneTag.ENERGY_REQUIRED_FOR_BIRTH);

         if ((days_pregnant >= gp) && health >= be) {

            giveBirth();
            pregnant = false;

         } else if (health < be) {
            // Mis-carry
            pregnant = false;
            System.out.println(this.toString() + " miscarries her baby after " + days_pregnant + " days. :(");
            days_pregnant = 0;
            tags.remove("Is pregnant.");
         } else {
            days_pregnant++;
         }
      }
   }
   /*
    * Begin: MOTION DYNAMICS, GEOMETRY METHODS
    * 
    */

   /**
    * Peform actions that happen as a result of our movement
    */
   public void movementConsequences() {
      if (Scenario.getEnvironment() instanceof TrailsEnvironment) {
         ((TrailsEnvironment) Scenario.getEnvironment()).blazeTrail(location.x, location.y);
      }
   }

   /**
    * Move straight towards a target.
    * 
    * @param other
    */
   protected void directlySeek(AbstractAgent other) {
      acceleration = track(other.getLocation());
      velocity.add(acceleration);
   }

   /**
    * The force that binds like individuals together
    * 
    * @param prefTargets
    * @return
    */
   protected PVector socialize(Collection<? extends Drawable> prefTargets) {

      PVector sum = new PVector(0, 0);
      int count = 0;
      for (Drawable other : prefTargets) {
         float d = PVector.dist(location, other.getLocation());

         if ((d > 0) && (d < getSociability())) {
            sum.add(other.getLocation());
            count++;
         }
      }
      if (count > 0) {
         sum.div(count);
         return track(sum);
      } else {
         return new PVector(0, 0);
      }
   }

   /**
    * Now updated for genetic variation. No need to speed limit anywhere else!
    * 
    * @since 0.3
    */
   public void applyMovement() {
      PVector lastposition = location;

      // Go as fast as we can!
      acceleration.mult(getMaxSpeed());

      // Update velocity
      velocity.add(acceleration);

      // BUT Limit to top speed
      velocity.limit(getMaxSpeed());
      location.add(velocity);

      float travel = PApplet.dist(lastposition.x, lastposition.y, location.x, location.y);

      // Moving comes at an energy cost!
      health -= (calorieCostPerPixel + getGenomicVarianceFor(GeneTag.CALORIE_COST_PER_PIXEL)) * travel;

      stopMoving();
   }

   protected void applyForce(PVector force) {
      // We could add mass here if we want A = F / M
      acceleration.add(force);
   }

   protected void stopMoving() {
      // Because we're now content for this cycle/epoch...
      // Clear all other priorities by resetting velocity and
      // acceleration
      velocity.mult(0);
      acceleration.mult(0);
   }

   /**
    * Separation function calcualtes the best vector to travel in the opposite
    * direction.
    * 
    * @param prefTargets
    * @return
    */
   protected PVector findPersonalSpace(Collection<? extends Drawable> prefTargets) {

      PVector steer = new PVector(0, 0, 0);
      int count = 0;

      for (Drawable other : prefTargets) {

         float d = PVector.dist(getLocation(), other.getLocation());

         // If there's anyone in your personalSpace find the best direction to
         // walk away
         if ((d > 0) && (d < getPersonalSpace())) {
            // Calculate vector pointing away from neighbor

            PVector diff = PVector.sub(getLocation(), other.getLocation());
            diff.normalize();
            diff.div(d); // Weight by distance

            steer.add(diff);
            count++; // Keep track of how many
         }
      }
      // Average -- divide by how many
      if (count > 0) {
         steer.div((float) count);
      }

      // As long as the vector is greater than 0
      if (steer.mag() > 0) {
         steer.normalize();
         steer.mult(getMaxSpeed()); // Move away as fast as you can

         steer.sub(velocity); // steer according to the current heading

         steer.limit(maxSteeringForce); // Turning speed in Radians!
      }
      return steer;
   }

   /**
    * Follow the crowd! In their wisdom they might know something we don't! This
    * could be an evolutionary advantage.
    * 
    * @param prefTargets
    * @return PVector
    */
   protected PVector align(Collection<? extends Drawable> prefTargets) {
      float neighbordist = 50;
      PVector sum = new PVector(0, 0);
      int count = 0;
      for (Drawable other : prefTargets) {
         float d = PVector.dist(location, other.getLocation());
         if ((d > 0) && (d < neighbordist)) {
            sum.add(other.getVelocity());
            count++;
         }
      }
      if (count > 0) {
         sum.div((float) count);

         sum.normalize();
         sum.mult(getMaxSpeed());
         PVector steer = PVector.sub(sum, velocity);
         steer.limit(maxSteeringForce);
         return steer;
      } else {
         return new PVector(0, 0);
      }
   }

   /**
    * @param prefTargets
    */
   public void generallyAimFor(Collection<? extends Drawable> prefTargets) {
      // Individuality
      PVector personal = findPersonalSpace(prefTargets);

      // Social cohesion
      PVector social = socialize(prefTargets);

      // Wisdom of the Crowds
      PVector crowd = align(prefTargets);

      // Arbritary weighting of these forces
      personal.mult(3.2f);
      social.mult(3.2f);
      crowd.mult(3.2f);

      // We could also add obstacle/threat avoidance and/or terrain-slowing here

      applyForce(personal);
      applyForce(social);
      applyForce(crowd);

   }

   /**
    * @param target
    * @return PVector
    */
   protected PVector track(PVector target) {
      PVector desired = PVector.sub(target, location);
      desired.setMag(getMaxSpeed());

      PVector steer = PVector.sub(desired, velocity);
      steer.limit(maxSteeringForce);
      return steer;
   }

   /**
    * Genetically modified maximum speed
    * 
    * @return the maxspeed
    * @since 0.3
    */
   protected float getMaxSpeed() {
      return maxSpeed + getGenomicVarianceFor(GeneTag.MAX_SPEED);
   }

   /*
    * 
    * Begin: GENETICS AND MORTALITY
    * 
    */

   /**
    * Impregnation
    */
   public void impregnateBy(ReproducingEntity partner) {
      if (getGender() == Gender.Female && !pregnant && age < menopause + getGenomicVarianceFor(GeneTag.MENOPAUSE)) {
         parent2 = partner;
         pregnant = true;
         days_pregnant++;
         this.tag("Is pregnant.");
         notifyObservers(new ScenarioEvent(EventType.PREGNANCY,
               String.format("%s is now pregnant with %s's baby.", this.toString(), partner.toString())));
      }
   }

   @Override
   public boolean isDead() {
      return !alive;
   }

   /**
    * Calls the consume(Entity by) function of this consumable resource.
    * 
    * @param consumable
    * @deprecated
    */
   public void consume(Consumable consumable) {
      // ie., if hungry...
      if (isHungry()) {
         consumable.consume();
      }
   }

   /**
    * Adds health in Calories, up to a limit Now updated for genetic variation
    * 
    * @since 0.3
    */
   public void addHealth(int additionalHealth) {
      if (health < geneticHealth())
         health += additionalHealth;
   }

   public String getCauseOfdeath() {
      return cause_of_death;
   }

   @Override
   public boolean isPregnant() {
      return pregnant;
   }

   @Override
   public int getDaysPregnant() {
      return days_pregnant;
   }

   @Override
   public synchronized void giveBirth() {

      pregnant = false;
      health -= energyRequiredForBirth;
      if (!isDead()) {
         ReproducingEntity baby;

         baby = new Harvester(getX(), getY());

         baby.setAge(0);

         baby.inheritGenes(this.getGenes()); // First insert probably just gets
         // dumped straight in
         baby.inheritGenes(parent2.getGenes()); // Second insert blends with
         // Genes
         // according to Gene.Type

         // Update the Lineage for this child.
         baby.getFamilyTree().add(parent2.getFamilyTree(), this.getFamilyTree());

         Scenario.getBirths().add(baby);

         this.tag("Gave birth!");

         this.tags.remove("Is pregnant.");
         days_pregnant = 0;

         notifyObservers(new ScenarioEvent(EventType.BIRTH,
               String.format("%s gives birth to: %s", this.toString(), baby.toString())));
      }
   }

   /**
    * Hungry is a fixed cutoff point between MAX_HEALTH and starvation. Now
    * adjusted for genetic variation
    * 
    * @return boolean
    * @since 0.3
    */
   public boolean isHungry() {

      // Hunger affects your mood...
      if (getHealth() < geneticHealth() * hungerThreshold) {
         sociability -= 0.5;
         personalSpace += 0.5;
         return true;
      } else {
         personalSpace = defaultPersonalSpace;
         sociability = defaultSociability;
         return false;
      }
   }

   /**
    * Starving is a fixed cutoff point between Hungry and Death.
    * 
    * @return boolean
    */
   public boolean isStarving() {
      return (health < geneticHealth() * starvationThreshold) ? true : false;
   }

   /**
    * @return int
    */
   public int geneticHealth() {
      return (int) baseHealth + (int) getGenomicVarianceFor(GeneTag.BASE_HEALTH);
   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.core.Genetics#getGenes()
    */

   @Override
   public List<Gene> getGenes() {
      return genome;
   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.core.Genetics#inheritGenes(java.util.List)
    */
   @Override
   public synchronized void inheritGenes(List<Gene> genes) {
      if (genome.size() != 0) {
         List<Gene> remaining = Collections.synchronizedList(new ArrayList<Gene>());
         remaining.addAll(genes);

         List<Gene> removals = Collections.synchronizedList(new ArrayList<Gene>());
         List<Gene> combined = Collections.synchronizedList(new ArrayList<Gene>());

         // We don't simply just addAll... because some traits blend
         for (Gene a : genes) {
            for (Gene b : genome) {

               if (a.getTag().equals(b.getTag())) {

                  // Select the dominant Gene or get the combined Gene
                  Gene g = Gene.trait(a, b);
                  if (g != null) {
                     remaining.remove(a);
                     removals.add(b);
                     combined.add(g);
                  }
               }
            }
         }
         genome.removeAll(removals);
         genome.addAll(remaining);
         genome.addAll(combined);
      } else {
         genome.addAll(genes);
      }
   }

   @Override
   public void add(Gene g) {
      if (genome.isEmpty()) {
         genome.add(g);
      } else {
         List<Gene> additional = new ArrayList<Gene>();
         List<Gene> removal = new ArrayList<Gene>();

         for (Gene b : genome) {
            if (b == g) {
               Gene x = Gene.trait(g, b);
               if (x != null) {
                  additional.add(x);
               }
               removal.add(b);
            }
         }
         if (additional.size() == 0)
            genome.add(g);

         genome.removeAll(removal);
         genome.addAll(additional);

      }
   }

   /**
    * Peforms once a year functions
    */
   public void birthday() {
      if (age >= lifespan + (int) getGenomicVarianceFor(GeneTag.LIFESPAN)) {
         die("Old age");
      } else
         age++;
   }

   /*
    * 
    * Begin: DRAWING / RENDERING METHODS
    * 
    */

   @Override
   public void render() {
      super.render();
      if (alive) {
         // showTags();

         // Entities are Gender specific dots 5x5
         parent.pushMatrix();
         parent.translate(getX(), getY());

         if (Engine.is3DAccelerated()) {
            // lift up a little in 3D mode
            parent.translate(0, 0, 3);
         }
         parent.strokeWeight(1);

         parent.noFill();

         range();

         parent.ellipse(0, 0, 5, 5);

         parent.fill(255, 100);
         if (isHungry()) {
            parent.ellipse(0, 0, 3, 3);
         }

         parent.popMatrix();
      } else {
         // Draw dead state
         parent.pushMatrix();
         parent.translate(getX(), getY());
         parent.strokeWeight(1);
         parent.stroke(0, 0, 0, 50);
         parent.fill(0, 0, 0, 100);
         parent.ellipse(0, 0, 3, 3);
         parent.popMatrix();
      }
   }

   /**
    * Basically a "deadline" in the true sense of the word. Movement beyond this
    * radius would result in death without further resources.
    * 
    * @return float
    * @since 0.3
    */
   public float getRange() {

      float caloriesPerPixel = calorieCostPerPixel + getGenomicVarianceFor(GeneTag.CALORIE_COST_PER_PIXEL);
      float dailyCosts = dailyCalorieCosts + getGenomicVarianceFor(GeneTag.DAILY_CALORIE_COSTS);

      return health / (caloriesPerPixel + dailyCosts);
   }

   /**
    * @return float
    */
   public float getExtendedRange() {
      return Math.min((getRange() * 1.1f),
            (Scenario.getEnvironment().getWidth() * Scenario.getEnvironment().getHeight()) / 3);
   }

   private void range() {
      // Show Walking distance
      // ie., radius before death

      if (getGender() == Gender.Female) {
         parent.stroke(215, 117, 195, 80);
         if (isStarving())
            parent.ellipse(0, 0, getRange(), getRange());
         parent.stroke(215, 117, 195, 30);
         if (isHungry())
            parent.ellipse(0, 0, getRange(), getRange());
         parent.fill(215, 117, 195, 200);
      } else {
         parent.stroke(79, 158, 215, 80);
         if (isStarving())
            parent.ellipse(0, 0, getRange(), getRange());
         parent.stroke(79, 158, 215, 30);
         if (isHungry())
            parent.ellipse(0, 0, getRange(), getRange());
         parent.fill(79, 158, 215, 200);
      }

   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.people.primitives.Agent#toString()
    */

   // @Override
   // public String toString() {
   // return "{" + super.toString() + ", \"mother\": \"" + getMothersName() +
   // "\", \"father\": " + getFathersName()
   // + "\"}";
   // }

   /**
    * @return the myMother
    */
   public String getMothersName() {
      return familyTree.mothersName();
   }

   /**
    * @return the myFather
    */
   public String getFathersName() {
      return familyTree.fathersName();
   }

   /**
    * @param age
    */
   public void setAge(int age) {
      this.age = age;
   }

   /**
    * @return String Male or Female
    */
   public String getGenderAsString() {
      return (gender == Gender.Male) ? "Male" : "Female";
   }

   /**
    * @return enum
    */
   public Gender getGender() {
      return gender;
   }

   /**
    * Uses the numberofEpochsPerYear constant to determine age
    * 
    * @return int age
    */
   public int getAge() {
      return (int) (age + getGenomicVarianceFor(GeneTag.AGE));
   }

   /**
    * Gets the current health as Calories
    * 
    * @return int
    */
   public int getHealth() {
      return health;
   }

   /**
    * @param gender
    */
   public void setGender(Gender gender) {
      this.gender = gender;
   }

   /**
    * @return float
    * @since 0.3
    */
   public float getMatingDistance() {
      return defaultMatingDistance + getGenomicVarianceFor(GeneTag.MATING_DISTANCE);
   }

   /**
    * Can't breed with family!
    * 
    * @since 0.3
    */
   @Override
   public boolean isFamily(ReproducingEntity s) {
      return (familyTree.isFamily(s) || s.getFamilyTree().isFamily(this));
   }

   /**
    * @return my family tree
    */
   public Lineage getFamilyTree() {
      return familyTree;
   }

   @Override
   public float getGenomicVarianceFor(GeneTag tag) {
      float val = 0;
      synchronized (genome) {
         for (Gene g : genome) {
            val += g.getValueFor(tag);
         }
      }
      return val;
   }
}
