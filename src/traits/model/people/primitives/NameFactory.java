package traits.model.people.primitives;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NameFactory {

   private static File males;
   private static File females;
   private int         caret;
   private Scanner     input;

   public NameFactory() {
      males = new File("src/library/Male.txt");
      females = new File("src/library/Female.txt");
      caret = 0;
   }

   private String getRandomName(File f) {

      if (!f.exists())
         System.exit(0);
      String newName = "";
      String nameCheck = "";
      try {
         input = new Scanner(f);
         int r = (int) (Math.random() * 500);
         int n = caret;
         do {
            nameCheck = input.nextLine();
            if (n == r)
               newName = nameCheck;
            n++;

         } while (input.hasNextLine() && newName == "");
         caret = n;
         input.close();
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }

      return newName;
   }

   public String getMaleName() {
      return getRandomName(males);
   }

   public String getFemaleName() {
      return getRandomName(females);
   }
}
