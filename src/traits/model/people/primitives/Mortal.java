/**
 * 
 */
package traits.model.people.primitives;

/**
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
@SuppressWarnings("javadoc")
public interface Mortal {

   public void addHealth(int h);

   public void die(String cause_of_death);

   public String getCauseOfdeath();

   public boolean isDead();

   public void giveBirth();
}
