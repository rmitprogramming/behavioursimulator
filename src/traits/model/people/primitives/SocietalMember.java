/**
 * 
 */
package traits.model.people.primitives;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import processing.core.PVector;
import traits.model.Scenario;
import traits.model.people.GroupTracking;

/**
 * @author anthonyrawlins
 */
public class SocietalMember extends ReproducingEntity implements GroupTracking {

   private Map<ReproducingEntity, Integer> familiarity;

   /**
    * @param x
    * @param y
    */
   public SocietalMember(float x, float y) {
      super(x, y);
      familiarity = Collections.synchronizedMap(new HashMap<ReproducingEntity, Integer>());

   }

   /**
    * @param loc
    */
   public SocietalMember(PVector loc) {
      super(loc);
   }

   @Override
   public int getFamiliarity(ReproducingEntity s) {
      if (isFamily(s))
         return Integer.MAX_VALUE;
      return (familiarity.containsKey(s)) ? 0 : familiarity.get(s);
   }

   @Override
   public boolean isStranger(ReproducingEntity s) {
      // TODO - a threshold value of familiarityScore
      return (familiarity.containsKey(s)) ? false : true;
   }

   @Override
   public void behave() {
      // Get all people in the area and increase you familiarity with them based
      // on distance
      /*
       * familiarityScore is a function of distance over time
       */
      survey();
      super.behave();
   }

   private void survey() {
      // Get all entities in your range and increment their familiarity score
      Scenario.getMortalAgents().stream().filter(p -> p instanceof ReproducingEntity).map(r -> (ReproducingEntity) r)
            .filter(r -> PVector.dist(getLocation(), r.getLocation()) <= getRange()).forEach(e -> {
               familiarity.putIfAbsent((ReproducingEntity) e, 0);
            });

      familiarity.forEach((k, v) -> v++);
   }
}
