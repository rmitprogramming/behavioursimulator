/**
 * 
 */
package traits.model.people.primitives;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import processing.core.PApplet;
import processing.core.PVector;
import traits.Engine;
import traits.model.ScenarioEvent;
import traits.model.core.Drawable;

/**
 * Agent represents the basic animated object that all other simulated objects
 * are derived from. Even Inanimate objects like Obstructions are based on this
 * class.
 * 
 * @author Anthony Rawlins
 */
public abstract class AbstractAgent implements Drawable {

   protected PApplet parent;

   protected float   angle;
   protected PVector location;
   protected PVector velocity;
   protected PVector acceleration;

   protected String name;
   static int       agentcounter = 0;

   protected Set<String>        tags;
   protected Set<AgentObserver> observers;

   /**
    * @param x
    * @param y
    */
   public AbstractAgent(float x, float y) {
      this.tags = Collections.synchronizedSet(new HashSet<String>());
      this.observers = Collections.synchronizedSet(new HashSet<AgentObserver>());

      this.name = "Generic Agent (" + agentcounter++ + ")";

      this.parent = Engine.appWrapper;

      location = new PVector(x, y);

      // no two agents start facing the same way
      angle = parent.random(PApplet.TWO_PI);
      acceleration = new PVector(0, 0);
      velocity = new PVector(PApplet.cos(angle), PApplet.sin(angle));

   }

   /**
    * @param loc
    */
   public AbstractAgent(PVector loc) {
      this(loc.x, loc.y);
   }

   /**
    * Registers a ScenarioObserver
    * 
    * @param a
    */
   public void registerObserver(AgentObserver a) {
      observers.add(a);
   }

   /**
    * Draws a String offset from the object to the screen. Relies on PApplet
    * methods from parent.
    */
   public void showTags() {
      parent.pushMatrix();
      parent.translate(getX(), getY());

      parent.fill(255, 100);
      if (!tags.isEmpty()) {
         parent.textSize(10);
         synchronized (tags) {
            for (String s : tags) {

               parent.translate(0, 12);
               parent.text(s, 0, 0);
            }
         }
      }
      parent.popMatrix();
   }

   public float getX() {
      return location.x;
   }

   public float getY() {
      return location.y;
   }

   /**
    * Sets the x - coordinate
    * 
    * @param newx
    */
   public void setX(float newx) {
      location.x = newx;
   }

   /**
    * Sets the y - coordinate
    * 
    * @param newy
    */
   public void setY(float newy) {
      location.y = newy;
   }

   public PVector getLocation() {
      return location;
   }

   public PVector getVelocity() {
      return velocity;
   }

   public void render() {
      // showTags();

      // Defaults to a small grey dot 5x5
      parent.pushMatrix();
      if (Engine.is3DAccelerated()) {
         parent.translate(getX(), getY(), 1);
      } else {
         parent.translate(getX(), getY());
      }
      parent.stroke(0, 30);
      parent.fill(0, 100);
      parent.ellipse(0, 0, 5, 5);
      parent.popMatrix();
   }

   /**
    * Adds a tag to this Agent object
    * 
    * @param s
    */
   public void tag(String s) {
      tags.add(s);
   }

   /**
    * Accessor for name field
    * 
    * @return String
    */
   public String getName() {
      return name;
   }

   /**
    * Sets the name of this Agent
    * 
    * @param name
    */
   public void setName(String name) {
      this.name = name; // TODO - validate.
   }

   @Override
   public String toString() {
      return name;
   }

   /**
    * Tells all observers an EventType has happened and which object caused it.
    * 
    * @param type
    * @param message
    */
   public void notifyObservers(ScenarioEvent e) {
      synchronized (observers) {
         for (AgentObserver s : observers) {
            s.handleAgentEvent(e);
         }
      }
   }

   /**
    * Placeholder for future behaviors to happen. Subclasses will override this.
    */
   public abstract void behave();

   /**
    * 
    * @param ao
    */
   public void removeObserver(AgentObserver ao) {
      synchronized (observers) {
         observers.remove(ao);
      }
   }
}
