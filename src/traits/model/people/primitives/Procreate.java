package traits.model.people.primitives;

@SuppressWarnings("javadoc")
public interface Procreate {
	public void impregnateBy(ReproducingEntity thefather);
	public boolean isPregnant();
	public int getDaysPregnant();
	public void giveBirth();
}
