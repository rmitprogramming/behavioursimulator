/**
 * 
 */
package traits.model.people.primitives;

import traits.model.ScenarioEvent;

/**
 * @author anthonyrawlins
 *
 */
public interface AgentObserver {
   /**
    * 
    * @param e
    */
   public void handleAgentEvent(ScenarioEvent e);
}
