/**
 * 
 */
package traits.model.people;

import java.util.Set;
import java.util.stream.Collectors;

import processing.core.PApplet;
import processing.core.PVector;
import traits.Engine;
import traits.model.Scenario;
import traits.model.ScenarioEvent;
import traits.model.ScenarioEvent.EventType;
import traits.model.core.Drawable;
import traits.model.core.GeneTag;
import traits.model.core.Resource;
import traits.model.core.Seeker;
import traits.model.environment.EdibleEnvironment;
import traits.model.people.primitives.ReproducingEntity;
import traits.model.people.primitives.SocietalMember;

/**
 * In this class we extend upon the basic life and death behaviors of an Entity.
 * We add new paradigms of seeking a generic food resource in order to survive
 * and the need to find a partner and reproduce.
 * 
 * @author Anthony Rawlins
 * @email s3392244@student.rmit.edu.au
 */
public class HunterGatherer extends SocietalMember implements Seeker {

   protected Resource          favouriteResource;
   protected ReproducingEntity partner;
   protected boolean           busyToday = false;

   /**
    * @param x
    * @param y
    */
   public HunterGatherer(float x, float y) {
      super(x, y);
   }

   /**
    * @param loc
    */
   public HunterGatherer(PVector loc) {
      this(loc.x, loc.y);
   }

   protected static Set<HunterGatherer> allHunterGatherers() {
      return Scenario.getMortalAgents().stream().filter(h -> h instanceof HunterGatherer).map(h -> (HunterGatherer) h)
            .collect(Collectors.toSet());
   }

   /**
    * By calling super.behave() we ensure we don't lose any behaviors that we
    * inherit from Entity.
    */
   @Override
   public void behave() {
      busyToday = false;
      if (isHungry())
         eat();

      // this.seekResource();
      this.seekPartner();
      this.procreate();
      this.movementConsequences();
      super.behave();
      applyMovement();
      stopMoving();
   }

   protected void eat() {
      Engine.getScenario();
      if (Scenario.getEnvironment() instanceof EdibleEnvironment) {
         if (isHungry()) {
            int healthGained = ((EdibleEnvironment) (Scenario.getEnvironment())).eatPixel((int) location.x,
                  (int) location.y);
            this.addHealth(healthGained);
            notifyObservers(new ScenarioEvent(EventType.CONSUMED,
                  String.format("%s ate and gained %d calories.", this.toString(), healthGained)));
         }
      }
   }

   private void procreate() {
      if (!busyToday)
         potentialPartners().stream().filter(p -> p.getGender() == Gender.Female)
               .filter(p -> !isFamily(p) && !p.isPregnant()).filter(p -> !p.busyToday)
               .filter(p -> (PApplet.dist(p.getX(), p.getY(), this.getX(), this.getY()) < (getMatingDistance())))
               .forEach(p -> {

                  notifyObservers(new ScenarioEvent(EventType.GETBUSY,
                        String.format("%s and %s get busy.", p.toString(), this.toString())));

                  if ((!p.busyToday || busyToday) && parent
                        .random(2 * (chanceOfConception + getGenomicVarianceFor(GeneTag.CHANCE_OF_CONCEPTION))) <= 1) {
                     if (p.getGender() == Gender.Female)
                        p.impregnateBy(this);
                     if (p.getGender() == Gender.Male)
                        this.impregnateBy(p);
                  }
                  p.busyToday = true;
                  busyToday = true;
               });
   }

   /**
    * @return Set
    */
   public Set<Resource> generallyPreferredResources() {

      return Scenario.getResources().stream()
            .filter(r -> (PApplet.dist(getX(), getY(), r.getX(), r.getY()) <= getExtendedRange()))
            .filter(r -> !r.depleted()).collect(Collectors.toSet());
   }

   /**
    * In our implementation of the Seeker interface we seek a generic food and a
    * suitable partner.
    */
   public void seekResource() {

      // Generally seek food
      Set<Resource> res = generallyPreferredResources();

      // Seek specific resource
      favouriteResource = nearest(res);

      /* Priorities of life */
      if (favouriteResource != null && !isStarving()) {
         stopMoving();
         directlySeek(favouriteResource); // Straight to target!

      } else if (res.size() > 0) {
         generallyAimFor(res);
      }
   }

   /**
    * 
    */
   public void seekPartner() {

      // Generally seek partners
      Set<HunterGatherer> hg = potentialPartners();

      // Seek specific partner
      HunterGatherer closer_partner = nearest(hg);
      if (closer_partner != null)
         partner = closer_partner;

      /* Priorities of life */
      if (partner != null && !isStarving()) {

         // Clear all other priorities by resetting velocity and acceleration
         stopMoving();
         directlySeek(partner); // Straight to target!

      } else if (!isHungry() && hg.size() > 0) {
         generallyAimFor(hg);

      } else {
         generallyAimFor(Scenario.getMortalAgents());
      }
   }

   /**
    * @return Set
    */
   protected Set<HunterGatherer> potentialPartners() {

      return Scenario.getMortalAgents().stream().filter(h -> h instanceof HunterGatherer).map(h -> (HunterGatherer) h)
            .filter(h -> h != this).filter(h -> !h.isDead()).filter(h -> !h.isPregnant()).filter(h -> !h.isStarving())
            .filter(h -> h.getGender() != this.getGender())
            .filter(h -> (PVector.dist(getLocation(), h.getLocation()) < getExtendedRange()))
            .filter(h -> h.getAge() >= sexualMaturity + getGenomicVarianceFor(GeneTag.SEXUAL_MATURITY))
            .filter(h -> h.getAge() <= menopause + getGenomicVarianceFor(GeneTag.MENOPAUSE))
            .collect(Collectors.toSet());

   }

   /**
    * A reference to our desired partner based on hardwired parameters. Future
    * derivations could call super.bestPartner() and apply further more specific
    * filtering like belonging to a particular race or holding a particular
    * trait etc.
    * 
    * @return Entity
    */
   public HunterGatherer closestPartner() {
      return nearest(potentialPartners());
   }

   /**
    * @param c
    * @return
    */
   @SuppressWarnings("unchecked")
   private <T> T nearest(Set<? extends Drawable> c) {
      Drawable d = null;
      float m = Float.MAX_VALUE;
      float n = Float.MAX_VALUE;
      for (Drawable t : c) {
         n = PVector.dist(location, ((Drawable) t).getLocation());
         if (n < m) {
            m = n;
            d = t;
         }
         ;
      }
      return (T) d;
   }

   /**
    * This implementation solely bases needs upon closest proximity of a generic
    * resource. Further subclasses can fine tune these requirements to influence
    * behavior based on other more obscure needs.
    */
   public Resource nearestResource() {
      return nearest(generallyPreferredResources());
   }

   /**
    * Renders a basic gender specific colored dot if we're still alive. Draws a
    * line to our preferred partner. and preferred food source
    */
   @Override
   public void render() {
      parent.pushMatrix();
      super.render();

      if (!isDead()) {
         parent.noFill();
         parent.strokeWeight(0.35f);
         if (getGender() == Gender.Female) {
            parent.stroke(215, 117, 195, 15);
         } else {
            parent.stroke(79, 158, 215, 15);
         }

         for (HunterGatherer pot : potentialPartners()) {

            if (Engine.is3DAccelerated()) {

               parent.pushMatrix();
               parent.translate(0, 0, 3);
               parent.beginShape();
               parent.vertex(getX(), getY());
               parent.bezier(getX(), getY(), 0, getX(), getY(), 15, pot.getX(), pot.getY(), 15, pot.getX(), pot.getY(),
                     0);
               parent.endShape();
               parent.popMatrix();
            } else {
               parent.line(getX(), getY(), pot.getX(), pot.getY());
            }
         }

         // // Indicate our preferred partner
         if (partner != null) {
            parent.pushMatrix();
            parent.line(getX(), getY(), partner.getX(), partner.getY());
            parent.popMatrix();
         }
         // //
         // // // Indicate preferred resource
         // if (favouriteResource != null) {
         // parent.pushMatrix();
         // parent.stroke(parent.color(42, 88, 24), 50);
         // parent.line(getX(), getY(), favouriteResource.getX(),
         // favouriteResource.getY());
         // parent.popMatrix();
         // }

         // flag();
      }
      parent.popMatrix();
   }

   protected void flag() {
      // Flag
      int c = parent.color(255, 255, 122);
      parent.stroke(c, 20);
      parent.pushMatrix();
      parent.translate(getX(), getY());
      parent.stroke(c, 100);
      parent.fill(c, 20);
      parent.strokeWeight(2.3f);
      parent.line(0, 0, 0, 0, 0, 7);

      parent.strokeWeight(1.0f);
      parent.popMatrix();

   }

   // @Override
   // public String toString() {
   //
   // String geneReadout;
   // List<String> geneSequence = new ArrayList<String>(genome.size());
   // for (Gene g : genome) {
   // geneSequence.add(g.toString());
   // }
   // geneReadout = String.join(",", geneSequence);
   //
   // return String.format(
   // "" + "{\"HunterGatherer\": {\n" + "\t\"parents\": %s,\n" + "\t\"name\":
   // %s,\n" + "\t\"age\": %d,\n" + "\t\"max speed\": %.2f,\n"
   // + "\t \"health\": %d/%d\n" + "\t \"Genome\": {%s}" + "\t}\n" + "}",
   // super.toString(), name, getAge(), (MAX_SPEED +
   // getGenomicVarianceForKey("max speed")), getHealth(), geneticHealth(),
   // geneReadout);
   // }
}
