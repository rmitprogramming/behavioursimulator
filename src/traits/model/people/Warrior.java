/**
 * 
 */
package traits.model.people;

import java.util.Set;

import processing.core.PApplet;
import processing.core.PVector;
import traits.model.people.primitives.ReproducingEntity;

/**
 * @author anthonyrawlins
 *
 */
public class Warrior extends HunterGatherer implements Combatant {

   int   defensePower;
   int   attackPower;
   int   endurance;
   float speed;

   /**
    * @param x
    * @param y
    */
   public Warrior(float x, float y) {
      super(x, y);
      // TODO Auto-generated constructor stub
   }

   /**
    * @param loc
    * @param parent
    */
   public Warrior(PVector loc, PApplet parent) {
      super(loc);
      // TODO Auto-generated constructor stub
   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.Combatant#sizeUpOpponent(traits.model.Entity)
    */
   @Override
   public void sizeUpOpponent(ReproducingEntity e) {
      // TODO Auto-generated method stub

   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.Combatant#attack(traits.model.Entity)
    */
   @Override
   public void attack(ReproducingEntity e) {
      // TODO Auto-generated method stub

   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.Combatant#joinForcesWith(java.util.Set)
    */
   @Override
   public void joinForcesWith(Set<? extends Combatant> army) {
      // TODO Auto-generated method stub

   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.Combatant#getArmy()
    */
   @Override
   public Set<? extends Combatant> getArmy() {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public void render() {

   }

}
