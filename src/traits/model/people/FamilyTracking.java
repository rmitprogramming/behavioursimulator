package traits.model.people;

import traits.model.people.primitives.ReproducingEntity;

/**
 * @author anthonyrawlins
 * @since 0.3
 */
public interface FamilyTracking {
   /**
    * @param s
    * @return boolean
    */
   public boolean isFamily(ReproducingEntity s);
}
