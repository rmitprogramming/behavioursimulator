package traits.model.people;

import traits.model.people.primitives.ReproducingEntity;

/**
 * @author anthonyrawlins
 * @since 0.3
 */
public interface GroupTracking extends FamilyTracking {

   /**
    * @param s
    * @return int
    */
   public int getFamiliarity(ReproducingEntity s);

   /**
    * @param s
    * @return boolean
    */
   public boolean isStranger(ReproducingEntity s);

}
