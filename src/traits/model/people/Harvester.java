package traits.model.people;

import processing.core.PVector;
import traits.model.Scenario;
import traits.model.environment.EdibleEnvironment;

/**
 * @author anthonyrawlins
 */
public class Harvester extends HunterGatherer {

   /**
    * @param x
    * @param y
    */
   public Harvester(float x, float y) {
      super(x, y);
   }

   /**
    * @param loc
    */
   public Harvester(PVector loc) {
      super(loc);
   }

   @Override
   public void behave() {

      if (isHungry()) {
         seekFood();
         applyMovement();
         eat();
         stopMoving();
      }
      super.behave();

   }

   private void seekFood() {
      if (Scenario.getEnvironment() instanceof EdibleEnvironment) {
         PVector move = bestdirection(location);
         // move.mult(getMaxSpeed());
         // move.limit(getMaxSpeed());

         setX((location.x + move.x));
         setY((location.y + move.y));
         // move.sub(velocity);
         // acceleration.add(move);
      }
   }

   protected PVector bestdirection(PVector location) {
      return ((EdibleEnvironment) Scenario.getEnvironment()).getGreenestPixelIn5x5Near(location);
      // return new PVector(Engine.appWrapper.random(2) - 1,
      // Engine.appWrapper.random(2) - 1);
   }

   protected PVector bestdirection(float x, float y) {
      return bestdirection(new PVector(x, y));
   }

}
