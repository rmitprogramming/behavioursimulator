package traits.model.people;

import processing.core.PVector;
import traits.Engine;

/**
 * @author anthonyrawlins
 * @since 0.3
 * @version 0.3
 */
public class ExtraPerceptive extends Harvester {

   private enum Direction {
      NORTH, SOUTH, EAST, WEST
   }

   // radar steps must be odd numer squared to get a meaningful result.
   private final static int radarsteps = 49; // ie., a 7x7 grid
   private final static int stepsize   = 5;                   // calling a
   // function
   // that's
   // based
   // on a 5x5

   /*
    * radarsteps and stepsize stack up so the area of "perception"
    * for this agent is potentially a (1,125 px) 35x35 pixel grid surrounding
    * our current location.
    * 
    */

   /**
    * @param x
    * @param y
    */
   public ExtraPerceptive(float x, float y) {
      super(x, y);
   }

   /**
    * @param loc
    */
   public ExtraPerceptive(PVector loc) {
      super(loc);
   }

   @Override
   public void behave() {
      radar(location.x, location.y);
      applyMovement();
      stopMoving();
      super.behave();
   }

   private void radar(float x, float y) {
      /*
       * Swing out in a spiral from current location looking for best food
       * source
       * 
       */
      int turn = 1;  // 1st turn
      int rx = (int) x;
      int ry = (int) y;
      int vcount = 1;

      Direction d = Direction.WEST;

      // for now, the best direction to go is nowhere
      PVector best = acceleration;

      // Begin sweeping out
      for (int r = 0; r < radarsteps; r++) {

         // If we've gotten to the point necessary: turn right.
         if (r % turn == 0) {
            turn++;
            switch (d) {
            case NORTH:
               d = Direction.EAST;
               break;
            case SOUTH:
               d = Direction.WEST;
               break;
            case EAST:
               d = Direction.SOUTH;
               break;
            case WEST:
               d = Direction.NORTH;
               break;
            }
         }

         switch (d) {
         case NORTH:
            ry -= stepsize;
            break;
         case SOUTH:
            ry += stepsize;
            break;
         case EAST:
            rx += stepsize;
            break;
         case WEST:
            rx -= stepsize;
            break;
         }

         // our radar is looking on the world stage, not off it.
         if (rx > 3 && ry > 3 && ry < Engine.getEnvironment().getHeight() - 3 && rx < Engine.getEnvironment().getWidth() - 3) {
            best.add(bestdirection(rx, ry));
            vcount++;
         }
         // If we aren't on the world stage, never mind and continue sweeping.
         // We'll come back to it eventually.

      }
      best.div(vcount); // take the average best direction for food.
      // best.normalize();
      // best.mult(getMaxSpeed());
      acceleration.add(best);
   }
}
