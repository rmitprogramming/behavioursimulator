package traits.model.people;

import java.util.Set;

import traits.model.people.primitives.ReproducingEntity;

@SuppressWarnings("javadoc")
public interface Combatant {
   void sizeUpOpponent(ReproducingEntity e);
   void attack(ReproducingEntity e);
   void joinForcesWith(Set<? extends Combatant> army);
   Set<? extends Combatant> getArmy();
}
