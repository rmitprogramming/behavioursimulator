package traits.model;

import com.cedarsoftware.util.io.JsonReader;

public class ScenarioLoader {
   public Scenario ScenarioLoader(String scene) {
      return (Scenario) JsonReader.jsonToJava(scene);
   }
}
