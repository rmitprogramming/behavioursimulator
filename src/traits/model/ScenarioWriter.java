/**
 * 
 */
package traits.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.cedarsoftware.util.io.JsonWriter;

import traits.Engine;
import traits.model.people.primitives.AbstractAgent;

/**
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class ScenarioWriter implements ScenarioObserver {

   private FileOutputStream scene;
   private FileOutputStream environment;
   private FileOutputStream agents;

   /**
    * @param folder
    */
   public ScenarioWriter(String folder) {
      try {
         scene = new FileOutputStream(new File(folder + "/scene.json"));
      } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      try {
         environment = new FileOutputStream(new File(folder + "/environment.json"));
      } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      try {
         agents = new FileOutputStream(new File(folder + "/agents.json"));
      } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   /**
    * 
    */
   public void exportScenarioAsJson() {

      try {

         JsonWriter sceneWriter = new JsonWriter(scene);
         sceneWriter.write(Engine.getScenario());
         sceneWriter.close();

      } catch (Exception e) {
         e.printStackTrace();
      }

      try {

         JsonWriter environmentWriter = new JsonWriter(environment);
         environmentWriter.write(Scenario.getEnvironment());
         environmentWriter.close();

      } catch (Exception e) {
         e.printStackTrace();
      }

      try {

         JsonWriter agentWriter = new JsonWriter(agents);
         for (AbstractAgent a : Scenario.getMortalAgents()) {
            agentWriter.write(a);
         }
         agentWriter.close();

      } catch (Exception e) {
         e.printStackTrace();
      }

   }

   // TODO - Other export options...
   /**
    * 
    */
   public void exportAsText() {
   }

   /**
    * 
    */
   public void summaryReport() {
   }

   @Override
   public void handleScenarioEvent(ScenarioEvent e) {
      switch (e.type) {
      case COMPLETE:
         System.out.println("Exporter was notified of completion. Writing Scenario state as json. ->>");
         exportScenarioAsJson();
         System.out.println("<-- Wrote Scenario state as json.");

         break;
      default:
         break;
      }
   }
}
