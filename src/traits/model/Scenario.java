package traits.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.json.JsonObject;

import traits.EngineEvent;
import traits.EngineObserver;
import traits.model.ScenarioEvent.EventType;
import traits.model.core.Renders;
import traits.model.core.Resource;
import traits.model.environment.Environment;
import traits.model.people.primitives.AbstractAgent;
import traits.model.people.primitives.AgentObserver;
import traits.model.people.primitives.Mortal;
import traits.model.people.primitives.ReproducingEntity;

/**
 * Scenario is our Model object. changes to the Scenario create logged events
 * and causes Observers to update Views of Drawable Objects.
 * 
 * @author Anthony Rawlins
 */
public class Scenario implements Renders, Runnable, AgentObserver, EngineObserver {

   /**
    * 
    */
   public static final int     epochsPerYear    = 365;
   /**
    * 
    */
   public static final boolean respawnResources = true;

   private static Environment           env;
   private static Set<AbstractAgent>    allAgents = Collections.synchronizedSet(new HashSet<AbstractAgent>());
   private static Set<AbstractAgent>    births    = Collections.synchronizedSet(new HashSet<AbstractAgent>());
   private static Set<AbstractAgent>    deaths    = Collections.synchronizedSet(new HashSet<AbstractAgent>());
   private static Set<ScenarioObserver> listeners = Collections.synchronizedSet(new HashSet<ScenarioObserver>());

   private SynchronousQueue<Object> preparedScenarioStatesQueue;
   private SynchronousQueue<Object> renderCompletionQueue;

   private static int CURRENT_EPOCH = 0;
   private static int EPOCHS        = 1;
   private boolean    running       = false;
   JsonObject         model;

   /**
    * 
    */
   public Scenario() {

      preparedScenarioStatesQueue = new SynchronousQueue<Object>(true); // Fair
      renderCompletionQueue = new SynchronousQueue<Object>(true); // Fair

      /*
       * Because we generate an environment dynamically at the moment there are
       * limitations on how small the playing field/stage/gridworld can be.
       * Anything smaller than 20x20 will result in divide by zero errors!
       * 
       * TODO - BUGFIX
       * 
       */

      env = new Environment(200, 200);
      // registerListener(this);
      registerListener((ScenarioObserver) env);
   }

   /**
    * @return Environment
    */
   public static Environment getEnvironment() {
      return env;
   }

   /**
    * Track this object in the Scenario
    * 
    * @param d
    */
   public void addAgent(AbstractAgent d) {
      d.registerObserver(this);
      synchronized (allAgents) {
         allAgents.add(d);
      }
   }

   /**
    * @param a
    */
   public void removeAgent(AbstractAgent a) {
      synchronized (allAgents) {
         if (allAgents.contains(a))
            allAgents.remove(a);
         synchronized (listeners) {
            if (listeners.contains(a))
               listeners.remove(a);
         }
      }
   }

   /**
    * @return Set<Agent>
    */
   public static Set<AbstractAgent> getMortalAgents() {
      return allAgents.stream().filter(e -> e instanceof Mortal).filter(e -> !((Mortal) e).isDead())
            .collect(Collectors.toSet());
   }

   /**
    * @return Set<Resource>
    */
   public static Set<Resource> getResources() {
      return allAgents.stream().filter(r -> r instanceof Resource).map(r -> (Resource) r).collect(Collectors.toSet());
   }

   /**
    * utility function
    * 
    * @deprecated
    * @param t
    * @return Set
    */
   @SuppressWarnings("unchecked")
   public <T> Set<T> getAllSimilar(T t) {
      Set<T> s = new HashSet<T>();
      for (AbstractAgent a : allAgents) {
         if (a.getClass().equals(t.getClass())) {
            s.add((T) a);
         }
      }
      return s;
   }

   /**
    * @return int
    */
   public int getEpochs() {
      return EPOCHS;
   }

   /**
    * An attempt at future-proofing
    * 
    * @return JsonObject
    */
   public JsonObject getModel() {
      return model;
   }

   /**
    * Only used in GUI mode because there's no need to draw backgrounds etc when
    * running in headless mode. Features 2-pass rendering that draws objects by
    * classification. (This could be expanded upon to draw objects on specific
    * layers.) Currently redraws the background Layer, then all agents are drawn
    * over the top. The update() and render() calls are called from different
    * threads! this will allow playback at a consistent framerate or no frame
    * rendering at all. This is a Consumer thread in the Producer/Consumer
    * Paradigm.
    */
   public void render() {

      // Draw the background layer
      env.render();

      // Only draw drawable objects.
      try {
         preparedScenarioStatesQueue.poll(40, TimeUnit.MILLISECONDS);
         {
            synchronized (allAgents) {
               allAgents.forEach(r -> r.render());
            }
         }
         ;
         renderCompletionQueue.offer(new Object(), 60, TimeUnit.MILLISECONDS);
         Thread.sleep(0);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   /**
    * The run() method Optimisation - only artificially slow the thread if we
    * can't keep up with rendering it. This is a Producer Thread in the
    * Producer/Consumer Paradigm.
    */
   @Override
   public void run() {
      do {

         if (!running) {
            try {
               Thread.sleep(1000);
            } catch (Exception e) {
               e.printStackTrace();
            }
         } else {
            broadcastScenarioEvent(
                  new ScenarioEvent(EventType.EPOCH_CHANGE, String.format("\nEpoch %s:\n-----\n", ++CURRENT_EPOCH)));

            try {

               renderCompletionQueue.poll(40, TimeUnit.MILLISECONDS);
               // Send an update request to all objects in the scenario
               // This is effectively incrementing our Epochs
               // or TICKing over.

               synchronized (allAgents) {
                  allAgents.forEach(a -> a.behave());
               }
               synchronized (listeners) {

                  deaths.forEach(d -> {
                     d.removeObserver(this);
                  });

                  deaths.clear();

                  births.forEach(b -> {
                     b.registerObserver(this);
                  });

                  allAgents.addAll(births);
                  births.clear();
               }
               synchronized (allAgents) {
                  allAgents.removeAll(deaths);
               }

               Thread.sleep(0);

            } catch (Exception e) {
               e.printStackTrace();
            }

         }
         try {
            preparedScenarioStatesQueue.offer(new Object(), 60, TimeUnit.MILLISECONDS);
         } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      } while (!finished());

      // Scenario has run its full course.
      // Tell everyone who cares.
      broadcastScenarioEvent(new ScenarioEvent(EventType.COMPLETE, null));
   }

   private void broadcastScenarioEvent(ScenarioEvent e) {
      for (ScenarioObserver so : listeners) {
         so.handleScenarioEvent(e);
      }
   }

   /**
    * @param i
    */
   public void setEpochs(int i) {
      // TODO Validate and error handling
      EPOCHS = i;
   }

   /**
    * @return boolean
    */
   public boolean finished() {
      return ((CURRENT_EPOCH < EPOCHS)) ? false : true;
   }

   /**
    * @return int
    */
   public int getCurrentEpoch() {
      return CURRENT_EPOCH;
   }

   /**
    * @param observer
    */
   public static void registerListener(ScenarioObserver observer) {
      listeners.add(observer);
   }

   /**
    * @param observer
    */
   public static void removeListener(ScenarioObserver observer) {
      listeners.remove(observer);
   }

   /**
    * @deprecated
    */
   @Override
   public float getX() {
      return 0;
   }

   /**
    * @deprecated
    */
   @Override
   public float getY() {
      return 0;
   }

   /**
    * @return int
    */
   public int getPopulation() {
      return (int) allAgents.stream().filter(e -> e instanceof ReproducingEntity)
            .filter(e -> !((ReproducingEntity) e).isDead()).count();
   }

   /**
    * @return the deaths
    */
   public static Set<AbstractAgent> getDeaths() {
      return deaths;
   }

   /**
    * @return the births
    */
   public static Set<AbstractAgent> getBirths() {
      return births;
   }

   // Passes all agent events along to scenario observers
   @Override
   public void handleAgentEvent(ScenarioEvent e) {
      broadcastScenarioEvent(e);
   }

   public void setEnvironment(Environment readObject) {
      env = readObject;
   }

   @Override
   public void handleEngineEvent(EngineEvent e) {

      switch (e.type) {
      case STOP:
         running = false;
         break;
      case PAUSE:
         running = !running;
         break;
      case START:
      case STARTUP:
         running = true;
         break;
      case RESET:
         break;
      default:
         break;
      }

   }

   public static void reset() {
      allAgents.clear();
      births.clear();
      deaths.clear();
      listeners.clear();
      CURRENT_EPOCH = 0;
   }

}
