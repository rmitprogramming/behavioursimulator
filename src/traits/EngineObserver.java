/**
 * 
 */
package traits;

/**
 * @author anthonyrawlins
 *
 */
public interface EngineObserver {
   /**
    * Tell interested parties an EngineEvent has occured.
    * 
    * @param e
    */
   public void handleEngineEvent(EngineEvent e);
}
