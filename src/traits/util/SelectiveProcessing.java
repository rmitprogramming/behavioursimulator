package traits.util;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author anthonyrawlins
 */
public interface SelectiveProcessing {

   /**
    * @param source
    * @param tester
    * @param mapper
    * @param block
    */
   public static <X, Y> void processElements(Iterable<X> source, Predicate<X> tester, Function<X, Y> mapper, Consumer<Y> block) {
      for (X p : source) {
         if (tester.test(p)) {
            Y data = mapper.apply(p);
            block.accept(data);
         }
      }
   }
}
