/**
 * 
 */
package traits;

/**
 * @author anthonyrawlins
 *
 */
public class EngineEvent {

   /**
    * 
    */
   public Type    type;
   private String message;

   /**
    * 
    * @param message
    */
   public EngineEvent(String message) {
      this.message = message;
   }

   /**
    * 
    * @param type
    * @param message
    */
   public EngineEvent(Type type, String message) {
      this.type = type;
      this.message = message;
   }

   public EngineEvent(Type type) {
      this.type = type;
      this.message = "";
   }

   /**
    * 
    * @return the message
    */
   public String getMessage() {
      return message;
   }

   /**
    * @author anthonyrawlins
    *
    */
   public static enum Type {
      /**
       * The Engine has loaded the Scenario successfully.
       */
      LOADED,

      /**
       * The Engine is ready to run the Scenario.
       */
      READY,

      /**
       * The Engine is currently running the Scenario
       */
      RUNNING,

      /**
       * The Scenario is currently paused by the Engine.
       */
      PAUSED,

      /**
       * The Scenario has failed for some reason.
       */
      FAILED,

      /**
       * The Engine has successfully completed the Scenario
       */
      COMPLETE,

      /**
       * 
       */
      LOG,

      STARTUP, STOP, PAUSE, START, RESET
   }

}
