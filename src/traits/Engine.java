package traits;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import processing.core.PApplet;
import traits.EngineEvent.Type;
import traits.model.Scenario;
import traits.model.ScenarioEvent;
import traits.model.ScenarioLogger;
import traits.model.ScenarioObserver;
import traits.model.ScenarioReader;
import traits.model.ScenarioWriter;
import traits.model.environment.Environment;

/**
 * Engine is our simulation/scenario controller.
 * 
 * @author Anthony Rawlins (s3392244@student.rmit.edu.au)
 */
public class Engine implements ScenarioObserver, EngineObserver {

   /**
    * @author anthonyrawlins
    */

   /**
    * Provides a static reference back to our controlling parent PApplet
    */
   public static PApplet appWrapper;

   private static Scenario       scenario;
   private static ScenarioReader importer;
   private static ScenarioWriter exporter;
   public static ScenarioLogger  logger;

   private static boolean P3DAccelerated    = false;
   private static boolean OpenGLAccelerated = false;

   static private String inputFolder  = "input";
   static private String logFolder    = "logs";
   static private String outputFolder = "output";

   /**
    * @param p3dAccelerated
    *           the p3DAccelerated to set
    */
   public static void setP3DAccelerated(boolean p3dAccelerated) {
      P3DAccelerated = p3dAccelerated;
   }

   /**
    * @param openGLAccelerated
    *           the openGLAccelerated to set
    */
   public static void setOpenGLAccelerated(boolean openGLAccelerated) {
      OpenGLAccelerated = openGLAccelerated;
   }

   private static Set<EngineObserver> engineObservers;

   Thread t1;

   /**
    * Our Engine needs to have a reference to the parent PApplet that created it
    * for later calls. Engine uses an MVC approach and operates as a controller
    * to the model. In our case a "Scenario".
    * 
    * @param scenarioFolder
    * @param app
    */
   public Engine(String scenarioFolder, PApplet app) {

      appWrapper = app;

      importer = new ScenarioReader(scenarioFolder + inputFolder);
      exporter = new ScenarioWriter(scenarioFolder + outputFolder);
      logger = new ScenarioLogger(scenarioFolder + logFolder);

      engineObservers = Collections.synchronizedSet(new HashSet<EngineObserver>());

   }

   /**
    * @param eo
    */
   public static void registerEngineObserver(EngineObserver eo) {
      engineObservers.add(eo);
   }

   /**
    * Uses the SimulationFileParser to load the scenario from the supplied JSON
    * data The scenario is then loaded into local variables
    * 
    */
   public void init() {
      try {
         Scenario s = importer.parseInputFiles();
         setScenario(s);

         Scenario.registerListener(this);
         Scenario.registerListener(exporter);
         registerEngineObserver(s);

         broadcastEngineEvent(new EngineEvent(Type.LOADED));
      } catch (Exception e) {
         e.printStackTrace();
         broadcastEngineEvent(new EngineEvent(Type.FAILED));
      }
      if (scenario == null) {
         broadcastEngineEvent(new EngineEvent(Type.FAILED));
      } else {
         broadcastEngineEvent(new EngineEvent(Type.READY));
      }
   }

   /**
    * Provides a way to get the width of the environment for the current
    * Scenario
    * 
    * @return int
    */
   public int getWidth() {
      return Scenario.getEnvironment().getWidth();
   }

   /**
    * Provides a way to get the height of the environment for the current
    * Scenario
    * 
    * @return int
    */
   public int getHeight() {
      return Scenario.getEnvironment().getHeight();
   }

   /**
    * Gets the number of Epochs to run for the current Scenario
    * 
    * @return int
    */
   public int getEpochs() {
      return scenario.getEpochs();
   }

   /**
    * A time-travelling function to see the state of the simulation at any
    * epoch. Not yet implemented.
    * 
    * @param i
    */
   public void getSimulationStateAtEpoch(int i) {
      // TODO
      // Jump to any point in the timeline
      // Simulation must have been completed first!
   }

   /**
    * Basic accessor function for the current Epoch
    * 
    * @return integer
    */
   public int getCurrentEpoch() {
      return scenario.getCurrentEpoch();
   }

   /**
    * @return Environment
    */
   public static Environment getEnvironment() {
      return Scenario.getEnvironment();
   }

   /**
    * This application is Multi-threaded - Thread 2 is started here Rendering is
    * independant on the Scenario running.
    */
   public void renderAll() {
      getScenario().render();
   }

   /**
    * Normally runs at FULL SPEED, as fast as the thread can run. This way when
    * running in headless mode, processing speed is not restricted by framerate
    * operating as a throttling bottleneck. When running in GUI mode we add a
    * sleep call to slow down the animation to human watchable speeds.
    */
   public void start() {
      broadcastEngineEvent(new EngineEvent(Type.STARTUP, "Scenario is starting...\n"));
      t1 = new Thread(getScenario());
      t1.start();
      broadcastEngineEvent(new EngineEvent(Type.RUNNING, "Scenario is now running...\n"));
   }

   public void stop() {
      broadcastEngineEvent(new EngineEvent(Type.STOP, "Scenario has stopped.\n"));
   }

   public void pause() {
      broadcastEngineEvent(new EngineEvent(Type.PAUSE, "Scenario is paused.\n"));
   }

   private void broadcastEngineEvent(EngineEvent e) {
      if (engineObservers != null) {
         for (EngineObserver o : engineObservers) {
            o.handleEngineEvent(e);
         }
      }
   }

   /**
    * Handle Events that affect the Scenario entirely. TODO - Low to high level
    * events enabling customized levels of logging detail
    */
   @Override
   public void handleScenarioEvent(ScenarioEvent e) {
      switch (e.type) {

      case UPDATE:
      case READY:
      case EPOCH_CHANGE:
         logger.log(Level.FINE, e.getMessage(), null);
         break;

      case GENOCIDE:
      case COMPLETE:
         logger.log(Level.ALL, e.getMessage(), null);
         broadcastEngineEvent(new EngineEvent(Type.COMPLETE, "\nThe Scenario is complete.\n"));

         break;

      case RESOURCE_DEPLETED:
      case CONFLICT:
         logger.log(Level.FINE, e.getMessage(), null);
         break;

      case FRAME_READY:
      case CONSUMED:
      case GETBUSY:
         logger.log(Level.FINEST, e.getMessage(), null);
         break;

      case NOTICE:
      case PREGNANCY:
      case BIRTH:
      case DEATH:
         logger.log(Level.FINER, e.getMessage(), null);
         break;

      default:
         break;
      }
   }

   /**
    * An Engine can only have 1 Scenario
    * 
    * @return static reference to the Scenario
    */
   public static Scenario getScenario() {
      return scenario;
   }

   /**
    * Statically sets the scenario for the whole Engine
    * 
    * @param scenario
    */
   public static void setScenario(Scenario scenario) {
      Engine.scenario = scenario;
   }

   /**
    * TODO This is a workaround to get v0.2 Prototype working
    * 
    * @return boolean true
    * @since 0.2
    */
   public static boolean is3DAccelerated() {
      return (P3DAccelerated || OpenGLAccelerated) ? true : false;
   }

   /**
    * @return the appWrapper
    */
   public PApplet getAppWrapper() {
      return appWrapper;
   }

   /**
    * @param appWrapper
    *           the appWrapper to set
    */
   public void setAppWrapper(PApplet appWrapper) {
      Engine.appWrapper = appWrapper;
   }

   /**
    * 0 to 100 % complete
    * 
    * @return int
    */
   public static int getPercentageComplete() {
      return (int) ((float) scenario.getCurrentEpoch() / (float) scenario.getEpochs() * 100);
   }

   @Override
   public void handleEngineEvent(EngineEvent e) {
      switch (e.type) {

      case LOG:
      case PAUSED:
      case STARTUP:
      case START:
      case STOP:
      case PAUSE:
      case READY:
      case RUNNING:
      case LOADED:
      case COMPLETE:
         logger.log(Level.INFO, e.getMessage(), null);
         break;

      // This is a more serious event
      case FAILED:
         logger.log(Level.SEVERE, e.getMessage(), null);
         break;

      default:
         break;

      }

   }

   public void reset() {
      Scenario.reset();
      init();
   }

}
