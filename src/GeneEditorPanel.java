
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeListener;

import traits.model.core.Gene;
import traits.model.core.Gene.Type;
import traits.model.core.GeneTag;

/**
 * @author anthonyrawlins
 *
 */
@SuppressWarnings("serial")
public class GeneEditorPanel extends JPanel {

   private Gene               g;
   private JComboBox<GeneTag> tagLabelComboBox;
   private JComboBox<Type>    geneTypeComboBox;
   private JSpinner           valueAdjuster;
   private JButton            duplicateButton;
   private JButton            deleteButton;

   @SuppressWarnings("javadoc")
   public GeneEditorPanel() {
      super();
      g = new Gene(GeneTag.BASE_HEALTH, Type.MAX, 0.0f);
      init();
   }

   public GeneEditorPanel(Gene g) {
      super();
      g = new Gene(g);
      init();
   }

   private void init() {
      setBorder(null);
      setBackground(new Color(211, 211, 211));

      JLabel tagIcon = new JLabel("");

      tagIcon.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/tag-2x.png")));
      add(tagIcon);

      tagLabelComboBox = new JComboBox<GeneTag>();
      tagLabelComboBox.setMaximumRowCount(20);
      tagLabelComboBox.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
      tagLabelComboBox.setModel(new DefaultComboBoxModel<GeneTag>(GeneTag.values()));
      add(tagLabelComboBox);
      tagLabelComboBox.addItemListener(new ItemListener() {
         @Override
         public void itemStateChanged(ItemEvent e) {
            g.setTag((GeneTag) e.getItem());
         }
      });

      geneTypeComboBox = new JComboBox<Type>();
      geneTypeComboBox.setMaximumRowCount(20);
      geneTypeComboBox.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
      geneTypeComboBox.setModel(new DefaultComboBoxModel<Type>(Type.values()));
      add(geneTypeComboBox);

      geneTypeComboBox.addItemListener(new ItemListener() {
         @Override
         public void itemStateChanged(ItemEvent e) {
            g.setType((Type) e.getItem());
         }
      });

      valueAdjuster = new JSpinner();
      valueAdjuster.setModel(new SpinnerModel() {

         @Override
         public Object getValue() {
            // TODO Auto-generated method stub
            return null;
         }

         @Override
         public void setValue(Object value) {
            // TODO Auto-generated method stub

         }

         @Override
         public Object getNextValue() {
            // TODO Auto-generated method stub
            return null;
         }

         @Override
         public Object getPreviousValue() {
            // TODO Auto-generated method stub
            return null;
         }

         @Override
         public void addChangeListener(ChangeListener l) {
            // TODO Auto-generated method stub

         }

         @Override
         public void removeChangeListener(ChangeListener l) {
            // TODO Auto-generated method stub

         }

      });
      add(valueAdjuster);

      duplicateButton = new JButton("");
      duplicateButton.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/share-2x.png")));
      add(duplicateButton);

      duplicateButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {

         }
      });

      deleteButton = new JButton("");
      deleteButton.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/circle-x-2x.png")));
      add(deleteButton);
   }
}