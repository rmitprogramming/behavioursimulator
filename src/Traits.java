
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import processing.core.PApplet;
import traits.Engine;
import traits.EngineEvent;
import traits.EngineObserver;
import traits.model.Scenario;

/**
 * Traits is a team project for RMIT University via Open Universities Australia
 * Traits.class is an Observer of our simulation model called a Scenario. The
 * simulation is managed by a Scenario Engine which is responsible for loading
 * Scenario parameters from a JSON-formatted run file.
 * 
 * @email s3392244@student.rmit.edu.au
 * @author Anthony Rawlins
 * @version 0.3
 */
@SuppressWarnings({ "unused", "serial" })
public class Traits extends PApplet implements EngineObserver {

   // Application name
   private final static String appname = "Traits ";
   private final static String version = "v0.3";

   // TODO - usage message formatting
   private static final String USAGE = "Usage: " + appname + "[--no-gui]" + " --input path/to/scenario/ ";

   // The Simulation Engine...
   private static Engine engine;

   // Defaults - overridden by command line options - modify to suit your
   // environment!
   static private String path = "src/scenarios/default/";

   /**
    * With Accel is set statically and publicly so each view can run 3D
    * rendering methods on demand, and at their discretion.
    */
   public static boolean with_accel = false;

   private static int     framerate   = 30;
   private static boolean with_opengl = false;
   private static int     smoothing   = 2;
   private static boolean no_gui      = false;
   private String         name;

   private long start_time      = 1;
   private long completion_time = 1;
   private long execution_time  = 0;

   private float          scaling      = 1.0f;
   private static boolean gui_ready    = false;
   private static boolean engine_ready = false;
   private static boolean file_loaded  = false;

   private float zrot           = 0;
   private float xrot           = 0;
   private float zoom           = 1.0f;
   private float xshift         = 0;
   private float yshift         = 0;
   private int   viewPortHeight = 550;
   private int   viewPortWidth  = 550;

   /**
    * @return the xshift
    */
   public float getXshift() {
      return xshift;
   }

   /**
    * @param xshift
    *           the xshift to set
    */
   public void setXshift(float xshift) {
      this.xshift = xshift;
   }

   /**
    * @return the yshift
    */
   public float getYshift() {
      return yshift;
   }

   /**
    * @param yshift
    *           the yshift to set
    */
   public void setYshift(float yshift) {
      this.yshift = yshift;
   }

   /**
    * @return the zrot
    */
   public float getZrot() {
      return zrot;
   }

   /**
    * @param zrot
    *           the zrot to set
    */
   public void setZrot(float zrot) {
      this.zrot = zrot;
   }

   /**
    * @return the xrot
    */
   public float getXrot() {
      return xrot;
   }

   /**
    * @param xrot
    *           the xrot to set
    */
   public void setXrot(float xrot) {
      this.xrot = xrot;
   }

   /**
    * @return the zoom
    */
   public float getZoom() {
      return zoom;
   }

   /**
    * @param zoom
    *           the zoom to set
    */
   public void setZoom(float zoom) {
      this.zoom = zoom;
   }

   /**
    * Default constructor for our Application
    * 
    * @since 0.2
    */
   public Traits() {
      engine = new Engine(path, this);
      name = appname + version;

      Engine.registerEngineObserver(this);
      engine.setAppWrapper(this);
   }

   /**
    * Overloaded constructor that sets alternative file locations.
    * 
    * @param scenarioFolder
    * 
    */
   public Traits(String scenarioFolder) {

      Traits.path = scenarioFolder;

      engine = new Engine(path, this);
      name = appname + version;

      Engine.registerEngineObserver(this);
      engine.setAppWrapper(this);

      if (no_gui) {
         engine.init();
         System.out.printf("Running in headless mode!\n");
         do {
            System.out.printf("%s", ".");
         } while (!engine_ready);
      }
   }

   /**
    * Allows Scenario Builder to run in 3D mode
    * 
    * @param b
    */
   public void setAccel(boolean b) {
      with_accel = b;
   }

   /**
    * Called once after the constructor only when running in GUI mode. Creates
    * the Frame and sets the rendering modes of Processing.
    */
   @Override
   public void setup() {

      try {
         engine.init();
      } catch (Exception e) {

      }
      if (!no_gui) {

         // Factors affecting GUI mode rendering only...
         rectMode(CORNER);

         if (with_accel) {
            // P3D accelerated
            size(engine.getWidth(), engine.getHeight(), P3D);
            Engine.setP3DAccelerated(true);
         } else if (with_opengl) {
            // OpenGL accelerated
            size(engine.getWidth(), engine.getHeight(), OPENGL);
            Engine.setOpenGLAccelerated(true);
         } else {
            // 2D Animation only
            size(engine.getWidth(), engine.getHeight());
         }

         viewPortWidth = engine.getWidth();
         viewPortHeight = engine.getHeight();

         smooth(smoothing); // Anti-aliasing
         frameRate(framerate); // Tells PApplet to draw this frequently

         engine.renderAll(); // Called once to kick things off...
         draw();
         gui_ready = true;
      }
      if (engine_ready) {
         engine.start();
         start_time = millis();
      }
   }

   /**
    * When running in GUI mode this method is called multiple times a second
    * depending on Frame Rate.
    */
   @Override
   public void draw() {
      if (!no_gui) {
         if (with_accel) {
            float mouseOffset;

            float rotation;
            float halfheight = viewPortHeight / 2;
            float halfwidth = viewPortWidth / 2;

            // 1st Layer
            background(25);

            pushMatrix();
            translate(halfwidth, halfheight);
            if (mousePressed) {
               mouseOffset = 0 - (mouseY) / 2;
               rotation = HALF_PI + (HALF_PI * (mouseOffset / halfheight));
               xrot = HALF_PI - rotation;
            }

            rotateX(xrot);
            rotateZ(radians(zrot));

            // Zoom factor
            scale(zoom);

            // pushMatrix();
            //

            // 2nd Layer
            pushMatrix();
            translate(0 - halfwidth, 0 - halfheight);
            translate(xshift, yshift);
            engine.renderAll();
            popMatrix();

            popMatrix();
         } else {
            // 2nd Layer
            pushMatrix();

            // Zoom factor
            scale(zoom);

            engine.renderAll();
            popMatrix();
         }

         // 3rd Layer - Layer Rendering, Scenario renders first, then Heads-Up
         // Display.
         drawHUD();
      }
   }

   public void keyPressed() {
      if (key == CODED) {
         if (keyCode == LEFT) {
            zrot--;
         } else if (keyCode == RIGHT) {
            zrot++;
         } else if (keyCode == UP) {
            zoom += 0.2f;
         } else if (keyCode == DOWN) {
            zoom -= 0.2f;
         }
      } else {
         zrot = 0;
      }
   }

   private void drawHUD() {
      pushMatrix();
      fill(255, 100); // White text by default
      textSize(12);
      noStroke();
      translate(0, 0, 20);
      translate(20, engine.getHeight() - 25);
      text("© 2015 RMIT University", 0, 0);

      translate(0, -25); // Text insertion point positioning
      text("by Glenn Florence, Anthony Rawlins & Ben Tierney", 0, 0);

      translate(0, -25); // Text insertion point positioning
      text(appname + version, 0, 0); // copyright watermark

      translate(0, -25); // Text insertion point positioning
      text("Current Epoch: " + engine.getCurrentEpoch() + "/" + engine.getEpochs() + " ("
            + engine.getCurrentEpoch() / Scenario.epochsPerYear + " years)", 0, 0);

      translate(0, -25); // Text insertion point positioning
      text("Population: " + String.format("%d", Engine.getScenario().getPopulation()), 0, 0);

      translate(0, -25); // Text insertion point positioning
      text("Frame rate: " + String.format("%.2f FPS", frameRate), 0, 0);
      popMatrix();
   }

   /**
    * Program entry point
    * 
    * @param passedArgs
    */
   static public void main(String[] passedArgs) {

      // Fullscreen use this line...
      // String[] appletArgs = new String[] { "--present",
      // "--window-color=#666666", "--stop-color=#670303",
      // "Traits" };

      // Windowed
      String[] appletArgs = new String[] { "--window-color=#666666", "--stop-color=#670303", "Traits" };

      // No parameters
      if (passedArgs == null || passedArgs.length == 0) {
         usage();
         System.exit(0);
      }

      // Single parameter: show usage
      // Single parameter: correct place but no further required parameters
      if (passedArgs.length == 1 && passedArgs[0].equals("-u")
            || passedArgs.length == 1 && passedArgs[0].equals("--no-gui")) {
         usage();
         System.exit(0);
      }

      // Multiple parameters
      if (passedArgs.length > 0 && passedArgs[0].equals("--no-gui")) {

         // Running in headless mode!
         // Directly process the passedArgs without launching the viewer!
         no_gui = true;
         parseCommands(passedArgs);

         if (path == null) {
            usage();
            System.exit(ERROR);
         } else {
            Traits t = new Traits(path);
         }

      } else if (passedArgs.length > 0) {
         // Windowed GUI mode
         parseCommands(passedArgs);
         PApplet.main(concat(appletArgs, passedArgs));
      } else {
         PApplet.main(appletArgs);
      }
   }

   /**
    * Parses the command-line arguments passed to the program
    * 
    * @param args
    */
   static public void parseCommands(String[] args) {

      /*
       * TODO - the following block needs to be revised to ensure the required
       * switches, flags and command line options are all present.
       * 
       * OR we need to provide some default locations for log file and output
       * files etc.
       */

      if (args.length >= 2) {
         int a = 0;

         do {
            /*
             * this is where we can look for acceptable command line arguments.
             * Assumes order of options not important, except where a key value
             * pair.
             */
            switch (args[a]) {

            case "--frame-rate":
            case "-f":
               // the next arg *should* be the frame rate!
               if (!(args.length >= a + 1)) {
                  usage();
                  System.exit(ERROR);
               } else {
                  framerate = Integer.parseInt(args[a + 1]);
               }
               break;

            case "--scenario-folder":
            case "--input":
            case "-i":
               // the next arg *should* be the run file!
               if (!(args.length >= a + 1)) {
                  usage();
                  System.exit(ERROR);
               } else {
                  path = args[a + 1];
               }
               break;

            case "--with-accel":

               with_accel = true;
               break;

            case "--with-opengl":
               with_opengl = true;
               break;

            case "--smooth":
               // the next arg *should* be the frame rate!
               if (!(args.length >= a + 1)) {
                  usage();
               } else {
                  // TODO - Validation!
                  // eg 2,3,4,8
                  smoothing = Integer.parseInt(args[a + 1]);
               }
               break;

            case "--no-gui":
               // Because --no-gui should be the first switch,
               // See line 124 above.
               no_gui = true;

               break;

            case "-u":
               usage();
               System.exit(0); // Not a failure, showing usage because
               // asked
               break;

            default:

            }
            a++;
         } while (a < args.length);

      } else {
         usage();
         System.exit(ERROR);
      }
   }

   /**
    * Displays the proper usage of the application as a message to the console.
    */
   static public void usage() {
      System.out.printf("\n%s\n\n", USAGE);
   }

   private void showCompletionMessage() {
      pushMatrix();

      translate(width / 2, height / 2);

      String m = "Scenario complete.";

      translate(0 - textWidth(m) / 2, 0);

      text(m, 0, 0);
      popMatrix();
   }

   @Override
   public void handleEngineEvent(EngineEvent e) {
      switch (e.type) {
      case LOADED:
         file_loaded = true;
         break;

      case READY:
         engine_ready = true;
         if (no_gui || (!no_gui && gui_ready)) {
            engine.start();
            start_time = millis();
         }
         break;
      case RUNNING:
         break;
      case PAUSED: // TODO
         break;
      case FAILED: // TODO - Proper Logging!
         break;
      case COMPLETE:

         completion_time = millis();
         execution_time = completion_time - start_time;

         Engine.logger.log(Level.ALL,
               String.format(
                     "Time to complete: "
                           + String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(execution_time),
                                 TimeUnit.MILLISECONDS.toSeconds(execution_time)
                                       - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(execution_time))),
               0, 0), null);

         showCompletionMessage();
         // System.exit(0);

         // TODO This causes the animation to stop even if we haven't drawn the
         // last few frames. Need a signal to say drawing is complete.
         // noLoop(); // Stop the animation thread - can restart it with loop()

         break;
      case LOG:
         break;
      default:
         break;
      }

   }

   public void setViewPortWidth(int width) {
      viewPortWidth = width;
   }

   public void setViewPortHeight(int height) {
      viewPortHeight = height;
   }

   public void start() {
      engine.start();
   }

   public void stop() {
      engine.stop();
   }

   public void pause() {
      engine.pause();
   }

   public void reset() {
      engine.reset();
   }

   public void record() {
   }
}
