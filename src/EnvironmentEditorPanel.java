import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

public class EnvironmentEditorPanel extends JPanel {

   private static EnvironmentEditor editor;

   public EnvironmentEditorPanel() {
      setMinimumSize(new Dimension(550, 550));
      try {
         editor = new EnvironmentEditor();
         editor.frameRate = 30.0f;
         editor.displayWidth = 550;
         editor.displayHeight = 550;
         editor.height = 550;
         editor.width = 550;
      } catch (Exception e) {
         e.printStackTrace();
      }
      setLayout(new BorderLayout(0, 0));
      add(editor);
   }

   public void init() {
      editor.init();
   }

}
