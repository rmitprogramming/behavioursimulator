
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.DefaultCaret;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import traits.Engine;
import traits.EngineEvent;
import traits.EngineObserver;
import traits.model.LogListener;
import traits.model.Scenario;
import traits.model.ScenarioEvent;
import traits.model.ScenarioObserver;

/**
 * 
 * @author anthonyrawlins
 * @since 0.3
 */
public class TraitsScenarioBuilder implements EngineObserver, ScenarioObserver, LogListener {

   private JFrame                              frame;
   private final static AboutView              about            = new AboutView();
   private final static EnvironmentEditorPanel environmentPanel = new EnvironmentEditorPanel();
   private JMenuItem                           mntmAboutTraits;
   private static JTextArea                    logConsole       = new JTextArea();
   private static Traits                       mainWindow       = new Traits();
   private JTextField                          noiseSeedValue;
   private String                              listenLevel;
   private static int                          noiseSeed        = 0;

   /**
    * Launch the application.
    * 
    * @param args
    */
   public static void main(String[] args) {

      EventQueue.invokeLater(new Runnable() {
         public void run() {
            try {
               System.setProperty("apple.laf.useScreenMenuBar", "true");
               System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Test");
               UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
               TraitsScenarioBuilder window = new TraitsScenarioBuilder();
               window.frame.setVisible(true);
            } catch (ClassNotFoundException e) {
               System.out.println("ClassNotFoundException: " + e.getMessage());
            } catch (InstantiationException e) {
               System.out.println("InstantiationException: " + e.getMessage());
            } catch (IllegalAccessException e) {
               System.out.println("IllegalAccessException: " + e.getMessage());
            } catch (UnsupportedLookAndFeelException e) {
               System.out.println("UnsupportedLookAndFeelException: " + e.getMessage());
            } catch (Exception e) {
               e.printStackTrace();
            }

         }
      });
   }

   /**
    * Create the application.
    */
   public TraitsScenarioBuilder() {

      initialize();

   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize() {
      listenLevel = Level.ALL.getName();
      try {
         new FileHandler("log");
      } catch (Exception e) {
         // System.err.printf("Cannot open log file. %s", e.getStackTrace());
      }

      frame = new JFrame();
      frame.setBounds(0, 0, 794, 742);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.getContentPane().setLayout(new BorderLayout(0, 0));

      JTabbedPane mainTabbedPane = new JTabbedPane(JTabbedPane.TOP);
      frame.getContentPane().add(mainTabbedPane, BorderLayout.CENTER);

      JPanel appletView = new JPanel();
      appletView.setLayout(new BorderLayout(0, 0));

      JPanel jsonView = new JPanel();
      jsonView.setLayout(new BorderLayout(0, 0));
      JEditorPane codeView = new JEditorPane();
      codeView.setMargin(new Insets(2, 8, 2, 13));
      codeView.setBackground(new Color(105, 105, 105));
      codeView.setForeground(new Color(220, 220, 220));
      codeView.setText(
            "{\n   \"Scenario\": {\n      \"name\": \"Scenario1\",\n      \"EnvironmentWidth\": 650,\n      \"EnvironmentHeight\": 650,\n      \"epochs\": 2500,\n      \"entities\": {\n          \"HunterGatherer\": {\n                 \"name\": \"Slow\",\n                 \"Genome\": {\n                    \"Gene\": {\"max speed\": 0.03},\n                    \"Gene\": {\"lifespan\": 60},\n                    \"Gene\": {\"sexual maturity\": 18}\n                  }\n              }\n          },\n           {\n          \"HunterGatherer\": {\n                 \"name\": \"Fast\",\n                 \"Genome\": {\n                    \"Gene\": {\"max speed\": 0.98},\n                    \"Gene\": {\"lifespan\": 60},\n                    \"Gene\": {\"sexual maturity\": 18}\n                  }\n              }\n          }\n      }\n   }\n}");

      jsonView.add(codeView);

      mainTabbedPane.addTab("Simulation", new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/beaker-2x.png")),
            appletView, null);

      JPanel agentView = new JPanel();
      mainTabbedPane.addTab("Agents", new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/people-2x.png")),
            agentView, null);
      agentView.setLayout(new BorderLayout(0, 0));

      JScrollPane scrollPane_2 = new JScrollPane();
      agentView.add(scrollPane_2);

      JPanel genesPanel = new GenesListPanel();
      genesPanel.setBorder(null);
      scrollPane_2.setViewportView(genesPanel);

      // TODO add new panel for each gene in the model

      JToolBar toolBar = new JToolBar();
      agentView.add(toolBar, BorderLayout.NORTH);

      JButton btnNewAgent = new JButton("New Agent");
      btnNewAgent.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/person-2x.png")));
      toolBar.add(btnNewAgent);

      JPanel environmentView = new JPanel();
      mainTabbedPane.addTab("Environment", new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/brush-2x.png")),
            environmentView, null);
      environmentView.setLayout(new BorderLayout(0, 0));

      JToolBar toolBar_4 = new JToolBar();
      environmentView.add(toolBar_4, BorderLayout.NORTH);

      JButton btnPaintMode = new JButton("Paint mode");
      btnPaintMode.setEnabled(false);
      btnPaintMode.setPressedIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/white/brush-2x.png")));
      toolBar_4.add(btnPaintMode);
      btnPaintMode.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/brush-2x.png")));

      JButton btnAdjustments = new JButton("Adjustments");
      btnAdjustments.setEnabled(false);
      btnAdjustments.setPressedIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/white/dial-2x.png")));
      btnAdjustments.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/dial-2x.png")));
      toolBar_4.add(btnAdjustments);

      JButton btnImportPhoto = new JButton("Import Photo");
      btnImportPhoto.setEnabled(false);
      btnImportPhoto.setPressedIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/white/camera-slr-2x.png")));
      btnImportPhoto.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/camera-slr-2x.png")));
      toolBar_4.add(btnImportPhoto);

      JButton btnLayerView = new JButton("Layer view");
      btnLayerView.setEnabled(false);
      btnLayerView.setPressedIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/white/layers-2x.png")));
      toolBar_4.add(btnLayerView);
      btnLayerView.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/layers-2x.png")));

      environmentView.add(environmentPanel, BorderLayout.CENTER);
      environmentPanel.init();

      JPanel panel = new JPanel();
      environmentView.add(panel, BorderLayout.SOUTH);

      JLabel lblNoiseSeed = new JLabel("Noise Seed");
      panel.add(lblNoiseSeed);

      noiseSeedValue = new JTextField();
      noiseSeedValue.setText(String.format("%d", noiseSeed));

      panel.add(noiseSeedValue);
      noiseSeedValue.setColumns(10);

      JButton btnGenerateRandom = new JButton("Generate Random");
      btnGenerateRandom
            .setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/arrow-circle-left-2x.png")));
      panel.add(btnGenerateRandom);

      btnGenerateRandom.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {
            noiseSeed = (int) Math.random() * 100000;
         }

      });

      JCheckBox chckbxUseNoiseSeed = new JCheckBox("Use Noise Seed");
      chckbxUseNoiseSeed.setSelected(true);
      panel.add(chckbxUseNoiseSeed);
      mainTabbedPane.addTab("Code", new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/code-2x.png")),
            jsonView, null);

      JToolBar toolBar_2 = new JToolBar();
      jsonView.add(toolBar_2, BorderLayout.NORTH);

      JButton btnValidate = new JButton("Validate");
      btnValidate.setEnabled(false);
      btnValidate.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/thumb-up-2x.png")));
      btnValidate.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
         }
      });

      JButton btnFormatter = new JButton("Format");
      btnFormatter.setEnabled(false);
      btnFormatter.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/align-left-2x.png")));
      toolBar_2.add(btnFormatter);
      toolBar_2.add(btnValidate);

      JButton btnExport = new JButton("Export");
      btnExport.setEnabled(false);
      toolBar_2.add(btnExport);
      btnExport.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/share-boxed-2x.png")));

      mainWindow.setSize(new Dimension(550, 550));
      mainWindow.setName("traitsView");
      mainWindow.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
      mainWindow.frameRate = 30.0f;
      mainWindow.setPreferredSize(new Dimension(550, 550));
      mainWindow.setBackground(Color.BLACK);

      appletView.addComponentListener(new ComponentListener() {

         @Override
         public void componentResized(ComponentEvent e) {
            mainWindow.setViewPortWidth(appletView.getWidth());
            mainWindow.setViewPortHeight(appletView.getHeight());

         }

         @Override
         public void componentMoved(ComponentEvent e) {
            // TODO Auto-generated method stub

         }

         @Override
         public void componentShown(ComponentEvent e) {
            // TODO Auto-generated method stub

         }

         @Override
         public void componentHidden(ComponentEvent e) {
            // TODO Auto-generated method stub

         }

      });

      appletView.add(mainWindow, BorderLayout.CENTER);

      JPanel controlPanel = new ControlPanel();
      appletView.add(controlPanel, BorderLayout.SOUTH);

      /***/

      JMenuBar menuBar = new JMenuBar();
      frame.setJMenuBar(menuBar);

      JMenu mnEnvironment = new JMenu("Environment");
      menuBar.add(mnEnvironment);

      JMenuItem mntmImportColorMap = new JMenuItem("Import...");
      mnEnvironment.add(mntmImportColorMap);

      JSeparator separator = new JSeparator();
      mnEnvironment.add(separator);

      mntmAboutTraits = new JMenuItem("About Traits");
      mntmAboutTraits.setAction(new ShowAboutWindow());
      mnEnvironment.add(mntmAboutTraits);

      JMenuItem mntmAboutScenarioBuilder = new JMenuItem("About Scenario Builder");
      mnEnvironment.add(mntmAboutScenarioBuilder);

      JMenu mnScenario = new JMenu("Scenario");
      menuBar.add(mnScenario);

      JMenuItem mntmCreateNewScenario = new JMenuItem("New");
      mnScenario.add(mntmCreateNewScenario);

      JSeparator separator_3 = new JSeparator();
      mnScenario.add(separator_3);

      JMenuItem mntmOpen = new JMenuItem("Open");
      mnScenario.add(mntmOpen);

      JSeparator separator_4 = new JSeparator();
      mnScenario.add(separator_4);

      JMenuItem mntmSave = new JMenuItem("Save");
      mnScenario.add(mntmSave);

      JMenuItem saveAsItem = new JMenuItem("Save As...");
      saveAsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
            (java.awt.event.InputEvent.SHIFT_MASK | (Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()))));
      saveAsItem.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            System.out.println("Save As...");
         }
      });
      mnScenario.add(saveAsItem);

      JMenu mnAgents = new JMenu("Agents");
      menuBar.add(mnAgents);

      JMenuItem mntmCreateNew = new JMenuItem("New Agent");
      mnAgents.add(mntmCreateNew);

      JSeparator separator_2 = new JSeparator();
      mnAgents.add(separator_2);

      JMenuItem mntmOpen_1 = new JMenuItem("Open");
      mnAgents.add(mntmOpen_1);

      JMenuItem mntmSave_1 = new JMenuItem("Save Agent");
      mnAgents.add(mntmSave_1);

      JSeparator separator_1 = new JSeparator();
      mnAgents.add(separator_1);

      JMenuItem mntmSaveAsTemplate = new JMenuItem("Save Agent as Template");
      mnAgents.add(mntmSaveAsTemplate);

      JMenuItem mntmTemplates = new JMenuItem("Templates");
      mnAgents.add(mntmTemplates);

      JMenu mnHelp = new JMenu("Help");
      menuBar.add(mnHelp);

      JMenuItem mntmOpenDocumentation = new JMenuItem("Open Documentation");
      mnHelp.add(mntmOpenDocumentation);
      mntmOpenDocumentation.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            try {
               openWebpage(new URI("https://groupbehaviour.wordpress.com/"));
            } catch (Exception err) {
               err.printStackTrace();
            }
         }
      });

      mainWindow.setAccel(true); // <----------- Enable 3D mode here
      mainWindow.setLayout(null);

      JPanel panel_1 = new JPanel();
      appletView.add(panel_1, BorderLayout.NORTH);
      panel_1.setPreferredSize(new Dimension(10, 80));
      panel_1.setLayout(null);

      JButton spinLeft = new JButton("");
      spinLeft.addMouseListener(new MouseAdapter() {
         @Override
         public void mousePressed(MouseEvent e) {
            mainWindow.setZrot(mainWindow.getZrot() - 0.5f);
         }
      });
      spinLeft.setBounds(6, 5, 28, 20);
      panel_1.add(spinLeft);
      spinLeft.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/action-undo-2x.png")));

      JButton spinRight = new JButton("");
      spinRight.addMouseListener(new MouseAdapter() {
         @Override
         public void mousePressed(MouseEvent e) {
            mainWindow.setZrot(mainWindow.getZrot() + 0.5f);
         }
      });
      spinRight.setBounds(67, 5, 28, 20);
      panel_1.add(spinRight);
      spinRight.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/action-redo-2x.png")));

      JButton btnLeft = new JButton("");
      btnLeft.setBounds(6, 30, 28, 20);
      panel_1.add(btnLeft);
      btnLeft.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/arrow-left-2x.png")));

      JButton btnUp = new JButton("");
      btnUp.setBounds(36, 5, 28, 20);
      panel_1.add(btnUp);
      btnUp.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/arrow-top-2x.png")));

      JButton btnDown = new JButton("");
      btnDown.setBounds(36, 54, 28, 20);
      panel_1.add(btnDown);
      btnDown.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/arrow-bottom-2x.png")));

      JButton btnHomeLocation = new JButton("");
      btnHomeLocation.setBounds(36, 30, 28, 20);
      panel_1.add(btnHomeLocation);
      btnHomeLocation.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setXrot(0);
            mainWindow.setZrot(0);
            mainWindow.setXshift(0);
            mainWindow.setYshift(0);
            mainWindow.setZoom(1.0f);
         }
      });

      btnHomeLocation.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/location-2x.png")));

      JButton btnRight = new JButton("");
      btnRight.setBounds(67, 30, 28, 20);
      panel_1.add(btnRight);
      btnRight.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/arrow-right-2x.png")));

      JButton btnZoomIn = new JButton("");
      btnZoomIn.setBounds(6, 54, 28, 20);
      panel_1.add(btnZoomIn);
      btnZoomIn.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/zoom-in-2x.png")));

      JButton btnZoomOut = new JButton("");
      btnZoomOut.setBounds(67, 54, 28, 20);
      panel_1.add(btnZoomOut);
      btnZoomOut.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/zoom-out-2x.png")));

      JPanel panel_2 = new JPanel();
      panel_2.setBorder(null);
      panel_2.setBounds(98, 5, 446, 69);
      panel_1.add(panel_2);
      panel_2.setLayout(new FormLayout(
            new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC,
                  FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
            new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                  FormSpecs.DEFAULT_ROWSPEC, }));

      JLabel lblNewLabel = new JLabel("Epochs");
      panel_2.add(lblNewLabel, "2, 2, right, default");

      JSlider slider = new JSlider();
      panel_2.add(slider, "4, 2, center, default");

      JLabel lblNewLabel_1 = new JLabel("5000");
      lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
      panel_2.add(lblNewLabel_1, "6, 2");

      JLabel lblEpochsPerYear = new JLabel("Epochs per Year");
      panel_2.add(lblEpochsPerYear, "2, 4, right, default");

      JSlider slider_1 = new JSlider();
      panel_2.add(slider_1, "4, 4, center, default");

      JLabel label = new JLabel("365");
      label.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
      panel_2.add(label, "6, 4");
      btnZoomOut.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setZoom(mainWindow.getZoom() - 0.25f);
         }
      });
      btnZoomIn.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setZoom(mainWindow.getZoom() + 0.25f);
         }
      });
      btnRight.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setXshift(mainWindow.getXshift() - 2.5f);
         }
      });
      btnDown.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setYshift(mainWindow.getYshift() - 2.5f);
         }
      });
      btnUp.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setYshift(mainWindow.getYshift() + 2.5f);
         }
      });
      btnLeft.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            mainWindow.setXshift(mainWindow.getXshift() + 2.5f);
         }
      });

      JPanel consoleView = new JPanel();
      mainTabbedPane.addTab("Log", new ImageIcon(TraitsScenarioBuilder.class.getResource("/gray/terminal-2x.png")),
            consoleView, null);
      consoleView.setLayout(new BorderLayout(0, 0));

      JScrollPane scrollPane_1 = new JScrollPane();

      consoleView.add(scrollPane_1, BorderLayout.CENTER);

      JPanel controlPanel2 = new ControlPanel();
      consoleView.add(controlPanel2, BorderLayout.SOUTH);

      JToolBar toolBar_3 = new JToolBar();
      consoleView.add(toolBar_3, BorderLayout.NORTH);
      logConsole.setMargin(new Insets(2, 8, 2, 13));
      scrollPane_1.setViewportView(logConsole);

      logConsole.setBackground(new Color(105, 105, 105));
      logConsole.setForeground(new Color(220, 220, 220));

      DefaultCaret caret = (DefaultCaret) logConsole.getCaret();
      caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

      JLabel lblLogLevel = new JLabel("Log Level");
      toolBar_3.add(lblLogLevel);

      JComboBox<String> comboBox = new JComboBox<String>();
      comboBox.addItemListener(new ItemListener() {
         public void itemStateChanged(ItemEvent e) {
            listenLevel = e.getItem().toString();
         }
      });
      comboBox.setModel(new DefaultComboBoxModel(
            new String[] { "ALL", "FINEST", "FINER", "FINE", "INFO", "WARNING", "SEVERE", "OFF" }));
      comboBox.setSelectedIndex(0);
      comboBox.addItemListener(new ItemListener() {

         @Override
         public void itemStateChanged(ItemEvent e) {
            listenLevel = (String) e.getItem();
         }

      });
      toolBar_3.add(comboBox);

      JButton btnSave = new JButton("Save");
      btnSave.setEnabled(false);
      btnSave.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {

         }

      });
      btnSave.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/document-2x.png")));
      toolBar_3.add(btnSave);

      JButton markerButton = new JButton("Insert Marker");
      markerButton.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/flag-2x.png")));
      toolBar_3.add(markerButton);
      markerButton.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            logConsole.append("\n******* " + dateFormat.format(date) + " *******\n");
         }

      });

      JButton btnClear = new JButton("Clear");
      btnClear.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {
            logConsole.setText("");
         }

      });
      btnClear.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/trash-2x.png")));
      toolBar_3.add(btnClear);

      Engine.registerEngineObserver(this);
      Engine.logger.registerLogListener(this);
      Scenario.registerListener(this);

      mainWindow.init();
      frame.pack();
   }

   private class ShowAboutWindow extends AbstractAction {
      /**
       * 
       */
      private static final long serialVersionUID = 6647912228871800918L;

      public ShowAboutWindow() {
         putValue(NAME, "About Traits");
         putValue(SHORT_DESCRIPTION, "Traits is an RMIT Programming Project");
      }

      public void actionPerformed(ActionEvent e) {
         about.setVisible(true);
      }
   }

   @Override
   public void handleEngineEvent(EngineEvent e) {
   }

   @Override
   public void handleScenarioEvent(ScenarioEvent e) {

   }

   /*******
    * 
    * Begin: Code from StackOverflow!
    * 
    * 
    * @param uri
    * 
    *****/
   private static void openWebpage(URI uri) {
      Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
         try {
            desktop.browse(uri);
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   /**
    * 
    * @param url
    */
   private static void openWebpage(URL url) {
      try {
         openWebpage(url.toURI());
      } catch (URISyntaxException e) {
         e.printStackTrace();
      }
   }

   @Override
   public void eavesdrop(Level lvl, String message) {
      if (lvl.intValue() >= Level.parse(listenLevel).intValue()) {
         logConsole.append(message + "\n");
      }
   }

   class ControlPanel extends JPanel {

      public ControlPanel() {
         super();
         initialize();
      }

      public void initialize() {
         setLayout(new BorderLayout(0, 0));

         JPanel toolbarPanel = new JPanel();
         add(toolbarPanel, BorderLayout.SOUTH);

         JButton resetBtn = new JButton("Reset");
         toolbarPanel.add(resetBtn);
         resetBtn.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/media-skip-backward-2x.png")));
         resetBtn.setToolTipText("Reset");
         resetBtn.setSelectedIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/action-undo-3x.png")));
         resetBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               mainWindow.reset();
            }
         });

         JButton btnStart = new JButton("Start");
         toolbarPanel.add(btnStart);
         btnStart.setToolTipText("Start");
         btnStart.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/media-play-2x.png")));
         btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               mainWindow.start();
            }
         });

         JButton btnPause = new JButton("Pause");
         toolbarPanel.add(btnPause);
         btnPause.setToolTipText("Pause");
         btnPause.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/media-pause-2x.png")));
         btnPause.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               mainWindow.pause();
            }
         });

         JButton btnStop = new JButton("Stop");
         toolbarPanel.add(btnStop);
         btnStop.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/media-stop-2x.png")));
         btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               mainWindow.stop();
            }
         });

         JButton btnRecord = new JButton("Record");
         toolbarPanel.add(btnRecord);
         btnRecord.setToolTipText("Record");
         btnRecord.setIcon(new ImageIcon(TraitsScenarioBuilder.class.getResource("/black/media-record-2x.png")));

         JPanel progressPanel = new JPanel();

         ScenarioProgressView progressBar = new ScenarioProgressView();
         progressBar.setPreferredSize(new Dimension(500, 20));
         progressPanel.add(progressBar);

         add(progressPanel, BorderLayout.NORTH);

         Scenario.registerListener(progressBar);
      }
   }
}
