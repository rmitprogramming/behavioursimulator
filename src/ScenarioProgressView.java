
/**
 * 
 */

import javax.swing.BoundedRangeModel;
import javax.swing.JProgressBar;

import traits.Engine;
import traits.model.ScenarioEvent;
import traits.model.ScenarioEvent.EventType;
import traits.model.ScenarioObserver;

/**
 * @author anthonyrawlins
 * @since 0.3
 */
public class ScenarioProgressView extends JProgressBar implements ScenarioObserver {

   /**
    * 
    */
   private static final long serialVersionUID = 1862816418379267301L;

   /**
    * 
    */
   public ScenarioProgressView() {
      super();
   }

   /**
    * @param orient
    */
   public ScenarioProgressView(int orient) {
      super(orient);
   }

   /**
    * @param newModel
    */
   public ScenarioProgressView(BoundedRangeModel newModel) {
      super(newModel);
   }

   /**
    * @param min
    * @param max
    */
   public ScenarioProgressView(int min, int max) {
      super(min, max);
   }

   /**
    * @param orient
    * @param min
    * @param max
    */
   public ScenarioProgressView(int orient, int min, int max) {
      super(orient, min, max);
   }

   /*
    * (non-Javadoc)
    * 
    * @see traits.model.ScenarioObserver#handleScenarioEvent(traits.model.
    * ScenarioEvent.EventType, java.lang.Object)
    */
   @Override
   public void handleScenarioEvent(ScenarioEvent e) {

      if (e.type == EventType.EPOCH_CHANGE) {

         int p = Engine.getPercentageComplete();
         int v = (int) (((float) p / 100) * (float) this.getMaximum());

         this.setValue(v);
         this.validate();
      }
   }

}
