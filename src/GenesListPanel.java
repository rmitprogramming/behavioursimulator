import java.awt.Font;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.UIManager;

public class GenesListPanel extends JPanel {

   public GenesListPanel() {
      super();
      init();
   }

   private void init() {

      Box vert1 = Box.createVerticalBox();
      add(vert1);
      JTextPane tipsPane = new JTextPane();
      tipsPane.setText("Add Genes to each Agent to modify their behaviour.");
      tipsPane.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
      tipsPane.setBackground(UIManager.getColor("window"));
      vert1.add(tipsPane);

      GeneEditorPanel geneEditorPanel = new GeneEditorPanel();
      vert1.add(geneEditorPanel);
   }

}
