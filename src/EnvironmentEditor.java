import processing.core.PApplet;
import traits.model.environment.Environment;

public class EnvironmentEditor extends PApplet {

   private Environment env;

   public EnvironmentEditor() {
      env = new Environment(550, 550, (PApplet) this);
   }

   public void setup() {
      size(550, 550, P3D);
      smooth(0);
      frameRate(30);
   }

   public void draw() {

      background(0);

      pushMatrix();
      translate(0, 0);
      env.render();
      popMatrix();
   }

}
